% This script differs from 'TempCorrFR' in that it will only use
% combinations of stimuli that evoked a significant response for the
% evoked. This should not be used for spontaneous as the "sensitive" field
% is only related to the evoked response.

%% First step is to calculate reproducibility from the raw data

load('C:\Users\mbeck\Desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted.mat');

%%

% % output organization
% % "Synch"
% % Synch{region}.filetypes{pairs}[date site unit1 unit2 PEAK unit1FR unit2FR (check if sig CCG)]
% %                                ... for each depvar
% % bandwidth, AUC, AUC of main peak

% Create the data structure

% bin = [0.001,0.002,0.005];
bin = 0.001;
Synch=cell(3,length(bin));

Window = sort([-0.1 0.1]);
dur = 0.15;
pattern = '';                   % These are to remove
pattern2 = '.';                   % These are to keep

for b = 1:length(bin)
    binWidth = bin(b);
    binNum = length(Window(1):binWidth:Window(2))-2;
    for R=1:3
        switch R
            case 1
                data = OT;
            case 2
                data = FL;
            case 3
                data = AAr;
        end
        
        D=[];
        
        for date=1:size(data,2)
            for site = 1:length(data{date})
                
                list=combnk(2:length(data{date}{site}),2);
                
                for pair = 1:size(list,1)
                    
                    disp(['region: ' num2str(R) '; date: ' num2str(date) '; site: ' num2str(site) '; pair: ' num2str(pair)]);
                    
                    unit1 = data{date}{site}{list(pair,1)};
                    unit2 = data{date}{site}{list(pair,2)};
                    
                    filetypes1 = fieldnames(unit1);
                    filetypes2 = fieldnames(unit2);
                    
                    filetypes = intersect(filetypes1,filetypes2);

                    idx = regexpi(filetypes,pattern);
                    idx = cellfun(@isempty,idx);
                    filetypes = filetypes(idx);
                    
                    idx = regexpi(filetypes,pattern2);
                    idx = ~cellfun(@isempty,idx);
                    filetypes = filetypes(idx);
                    
                    clear filetypes1 filetypes2 idx
                    
                    for file = 1:length(filetypes)
                        
                        CCG = zeros(length(unit1.(filetypes{file}).depvar),binNum);
                        shiftCCG = zeros(length(unit1.(filetypes{file}).depvar),binNum);
                        
                        for depvar = 1:length(unit1.(filetypes{file}).depvar)
                            
                            % This loop means only combinations of stimuli that
                            % evoked a significant response for both units will
                            % be included in th analysis
                            
                            if unit1.(filetypes{file}).sensitive(depvar) && unit2.(filetypes{file}).sensitive(depvar)
                                for trial = 1:unit1.(filetypes{file}).reps
                                    if ~isempty(unit1.(filetypes{file}).spikes_times{depvar,trial})
                                        for spike = 1:length(unit1.(filetypes{file}).spikes_times{depvar,trial})
                                            
                                            times = unit1.([filetypes{file}]).spikes_times{depvar,trial}(spike) - unit2.([filetypes{file}]).spikes_times{depvar,trial};
                                            his=hist(times,Window(1):binWidth:Window(2)); %% Here is where you can change binwidth
                                            CCG(depvar,:)=CCG(depvar,:)+his(2:end-1);
                                            if trial==unit1.([filetypes{file}]).reps
                                                times = unit1.([filetypes{file}]).spikes_times{depvar,trial}(spike) - unit2.([filetypes{file}]).spikes_times{depvar,1};
                                            else
                                                times = unit1.([filetypes{file}]).spikes_times{depvar,trial}(spike) - unit2.([filetypes{file}]).spikes_times{depvar,trial+1};
                                            end
                                            his=hist(times,Window(1):binWidth:Window(2));
                                            shiftCCG(depvar,:)=shiftCCG(depvar,:)+his(2:end-1);
                                            
                                        end
                                        
                                    end
                                end % for "trial"
                                
                            end
                        end     % for "depvar" the first time
                        
                        clear his spike times
                        
                        holder = cell(1,9);
                        holder{1} = [date,site,list(pair,1),list(pair,2)];
                        for i = 2:8
                            holder{i} = nan(1,length(unit1.([filetypes{file}]).depvar));
                        end
                        holder{9}=cell(1,length(unit1.([filetypes{file}]).depvar));
                        
                        for depvar = 1:length(unit1.([filetypes{file}]).depvar)
                            
                            rate1=mean(unit1.([filetypes{file}]).spikes_count(depvar,:))/dur;
                            rate2=mean(unit2.([filetypes{file}]).spikes_count(depvar,:))/dur;
                            
                            SmCCG=(smooth(CCG(depvar,:))/sqrt(rate1*rate2))/binNum;
                            SmShiftCCG=(smooth(shiftCCG(depvar,:))/sqrt(rate1*rate2))/binNum;
                            
                            SmCCGCorrected=SmCCG-SmShiftCCG;
                            
                            flankS=std([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                            flankM=mean([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                            
                            PEAK=SmCCGCorrected(round(binNum/2));
                            check = PEAK>flankM+5*flankS;
                            
                            positive=(SmCCGCorrected>=(PEAK/2));
                            pos=diff(positive);
                            less=pos(1:round(binNum/2));
                            more=pos(round(binNum/2):end);
                            Low=find(less==1,1,'last');
                            High=find(more==-1,1,'first');
                            Low=length(less)-Low;
                            bandPEAK=High+Low;
                            
                            if isempty(bandPEAK)
                                bandPEAK=nan;
                                sumband=nan;
                            else
                                sumband=sum(SmCCGCorrected(length(less)-Low:length(less)+High))/bandPEAK;
                            end
                            
                            holder{2}(1,depvar)=PEAK;
                            holder{3}(1,depvar)=rate1;
                            holder{4}(1,depvar)=rate2;
                            holder{5}(1,depvar)=check;
                            holder{6}(1,depvar)=bandPEAK;
                            holder{7}(1,depvar)=sum(SmCCGCorrected);
                            holder{8}(1,depvar)=sumband;
                            holder{9}{1,depvar}=SmCCG;
                            holder{10}{1,depvar}=SmShiftCCG;
                            holder{11}{1,depvar}=SmCCGCorrected;
                            
                            clear positive pos less more Low High bandPEAK
                            
                        end     % for "depvar" the second time
                        
                        if isfield(D,([filetypes{file}]))
                            for i = 1:8
                                if size(D.(filetypes{file}){i},2) > size(holder{i},2)
                                    padding = nan(1,size(D.(filetypes{file}){i},2)-size(holder{i},2));
                                    D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i},padding];
                                elseif size(D.(filetypes{file}){i},2) < size(holder{i},2)
                                    padding = nan(size(D.(filetypes{file}){i},1),size(holder{i},2)-size(D.(filetypes{file}){i},2));
                                    D.([filetypes{file}]){i}=[D.([filetypes{file}]){i},padding;holder{i}];
                                else
                                    D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i}];
                                end
                            end
                            for i = 9:11
                                if size(D.(filetypes{file}){i},2) > size(holder{i},2)
                                    padding = cell(1,size(D.(filetypes{file}){i},2)-size(holder{i},2));
                                    D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i},padding];
                                elseif size(D.(filetypes{file}){i},2) < size(holder{i},2)
                                    padding = cell(size(D.(filetypes{file}){i},1),size(holder{i},2)-size(D.(filetypes{file}){i},2));
                                    D.([filetypes{file}]){i}=[D.([filetypes{file}]){i},padding;holder{i}];
                                else
                                    D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i}];
                                end
                            end
                        else
                            for i = 1:length(holder)
                                D.([filetypes{file}]){i}=holder{i};
                            end
                        end
                        
                        clear holder depvar rate* i PEAK check SmCCG curves CORR trial flank* sumband SmShiftCCG shiftCCG SmCCGCorrected CCG
                        
                    end         % for "file"
                    
                    clear filetypes
                    
                end             % for "pair"
            end                 % for "site"
        end                     % for "date"
        
        Synch{R,b} = D;
        % Synch{1} = D;
        
        clear date file site pair list unit1 unit2 data D
        
    end                         % for "region"
end                         % for "bin"
clearvars -except Synch* AAr OT FL
SynchEvoked = Synch;
clear Synch
%% extract the data from the "Synch" structure made above

% "data" structure

load('Synch_02.mat','Synch');

DATA_SYNandFR=cell(1,1);
KEY=cell(1,1);

for R = 1:3
    data = Synch{R};
    filetypes = fieldnames(data);
    
    for file = 1:length(filetypes)
        info = data.([filetypes{file}]){1};
        for pair = 1:size(data.([filetypes{file}]){1,1},1)
            check=data.([filetypes{file}]){5}(pair,:)==0;
            syn=data.([filetypes{file}]){2}(pair,:);
            band=data.([filetypes{file}]){6}(pair,:);
            auc=data.([filetypes{file}]){7}(pair,:);
            aucpeak=data.([filetypes{file}]){8}(pair,:);
            FR1=data.([filetypes{file}]){3}(pair,:)/0.15;
            FR2=data.([filetypes{file}]){4}(pair,:)/0.15;
            FR=nan(size(FR1));
            for i=1:length(FR(:))
                FR(i)=sqrt(FR1(i)*FR2(i));
            end
            %             noise=data.([filetypes{file}]){9}(pair,:);
            curve=data.([filetypes{file}]){9}(:,pair);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Use this to either eliminate non-responsive stimuli or not
            % either making it a nan or zero, or including
            correct = 0;
            
            syn(check)=correct;
            band(check)=correct;
            auc(check)=correct;
            aucpeak(check)=correct;
            %             FR(check)=correct;
            %             FR1(check)=correct;
            %             FR2(check)=correct;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            holder{1} = syn;
            holder{2} = band;
            holder{3} = auc;
            holder{4} = aucpeak;
            holder{6} = FR;
            holder{7} = FR1;
            holder{8} = FR2;
            holder{5} = curve;
            
            if isfield(DATA_SYNandFR{R},([filetypes{file}]))
                for i = 1:length(holder)
                    DATA_SYNandFR{R}.([filetypes{file}]){i}=[DATA_SYNandFR{R}.([filetypes{file}]){i};holder{i}];
                end
            else
                for i = 1:length(holder)
                    DATA_SYNandFR{R}.([filetypes{file}]){i}=holder{i};
                end
            end
            KEY{R}.([filetypes{file}])=info;
            
        end             % for "pair"
    end                 % for "file"
end                     % for "region"

clear check rep FR file pair region data filetypes noise FR1 FR2 i info band syn auc* unit1 unit2 Synch* correct holder

%% Merge the individual filetypes into the broader files

itd={'itd','ITD'};
ild={'ild','ILD'};
ff={'FF1','FF2','FF3','FF4','FF5','FFf1','FFf2','FFf3','FFf4','FFf5'};

ILD=cell(1,1);
ITD=cell(1,1);
FF=cell(1,1);

for curve = 1:3;
    if curve == 1; C=itd; end
    if curve == 2; C=ild; end
    if curve == 3; C=ff; end
    
    D=cell(3,8);
    K=cell(3,1);
    
    for R = 1%:3;
        
        for file = 1:length(C);
            if isfield(DATA_SYNandFR{R},([C{file}]));
                for met = 1:size(D,2);
                    D{R,met}=[D{R,met};DATA_SYNandFR{R}.([C{file}]){met}];
                end
                K{R}=[K{R};KEY{R}.([C{file}])];
            end
        end             % for "file"
    end                 % for "region"
    
    if curve == 1; ITD=D; KEY_ITD=K; end
    if curve == 2; ILD=D; KEY_ILD=K; end
    if curve == 3; FF=D; KEY_FF=K; end
    
end                     % for "curve"

clear curve region file itd ild ff D C DATA_SYNandFR met KEY* K

save SynchFR_temp

