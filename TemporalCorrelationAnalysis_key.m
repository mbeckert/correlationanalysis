function [OUTPUT,Pdata]=TemporalCorrelationAnalysis_key

% AAr
% FILES = {'data_006_2015_0502','data_006_2015_0521','data_006_2015_0616','data_006_2015_0713','data_006_2015_0722', ...
%     'data_021_2015_0528','data_021_2015_0609','data_021_2015_0706','data_021_2015_0721','data_021_2015_0730', ...
%     'data_023_2015_0422','data_023_2015_0501','data_023_2015_0610', ...
%     'data_029_2015_0910','data_029_2015_0929', ...
%     'data_032_2015_0903'};

% OT
% FILES = {'data_029_2015_1029','data_029_2015_1112','data_029_2015_1123','data_029_2015_1214', ...
%     'data_032_2015_1110','data_032_2015_1119','data_032_2016_0223'};

% Field L
FILES = {'data_029_2016_0113','data_029_2016_0510', ...
    'data_032_2016_0106','data_032_2016_0119','data_032_2016_0202','data_032_2016_0503','data_032_2016_0517'};

%% Set Up

files={'abi','ild','itd', ...
    'ILDp40','ILDp35','ILDp30','ILDp25','ILDp20','ILDp15', 'ILDp10','ILDp05', 'ILD000', ...
    'ILDn05','ILDn10','ILDn15','ILDn20','ILDn25','ILDn30','ILDn35','ILDn40', ...
    'ILD','ITD','FF1','FF2','FF3','FF4','FF5'};

OUTPUT=[];
OutputCounter=1;

Pdata=cell(1,3);

for date = 1:length(FILES);

    disp(['Loading File ' FILES{date}]);
    load([FILES{date} '.mat']);
    eval(['d=' FILES{date} ';']);
    eval(['clear ' FILES{date}]);

    for site = 1:length(d);

        data=cell(0);

        for filetype=1:length(files);
            checker=0;
            for h=1:size(d{site,1},1)-1;
                if isfield(d{site,1}{h+1,1},([files{filetype}]));
                    checker=checker+1;
                end
            end
            if checker>0;
                list=1:size(d{site,1},1)-1;
                for h=1:size(d{site,1},1)-1;
                    if ~isfield(d{site,1}{h+1,1},[files{filetype}])
                        list(h)=0;
                    end
                end
                list(list==0)=[];
                list=combnk(list,2);

                %% Perform Temporal Correlation

                disp('Running Temporal Correlation');
                for pair=1:size(list,1);
                    TempCorr=zeros([size(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times,1) 10]);
                    H=cell([size(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times,1) size(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times,2)]);
                    SHIFT=cell([size(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times,1) size(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times,2)-1]);
                    trial=0;
                    trialmax=size(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times,1)*size(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times,2);
                    while trial<trialmax;
                        trial=trial+1;
                        [indR,indC]=ind2sub(size(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times),trial);
                        if sum(isnan(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times{indR,indC}))>0 || sum(isnan(d{site,1}{list(pair,2)+1,1}.([files{filetype}]).spikes_times{indR,indC}))>0
                            H{trial}=zeros([1 199]);
                        else
                            H{trial}=zeros([1 199]);
                            for l=1:length(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times{indR,indC});
                                x=d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times{indR,indC}(l)-d{site,1}{list(pair,2)+1,1}.([files{filetype}]).spikes_times{indR,indC};
                                temphist=hist(x,-0.1:0.001:0.1);
                                H{trial}=H{trial}+temphist(2:end-1);
                            end
                        end
                    end
                    trial=0;
                    while trial<trialmax-size(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times,1);
                        trial=trial+1;
                        [indR,indC]=ind2sub(size(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times),trial);
                        if sum(isnan(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times{indR,indC}))>0 || sum(isnan(d{site,1}{list(pair,2)+1,1}.([files{filetype}]).spikes_times{indR,indC+1}))>0
                            SHIFT{trial}=NaN([1 199]);
                        else
                            SHIFT{trial}=zeros([1 199]);
                            for l=1:length(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times{trial});
                                x=d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_times{indR,indC}(l)-d{site,1}{list(pair,2)+1,1}.([files{filetype}]).spikes_times{indR,indC+1};
                                temphist=hist(x,-0.1:0.001:0.1);
                                SHIFT{trial}=SHIFT{trial}+temphist(2:end-1);
                            end
                        end
                    end

                    j=0;
                    SmCCGCorrectedS=zeros(199,1);
                    SmCCGS=zeros(199,1);
                    ShiftCCGS=zeros(199,1);
                    while j<size(H,1);
                        j=j+1;
                        holder=zeros([size(H,2) 199]);
                        holderSHIFT=zeros([size(SHIFT,2) 199]);
                        for i=1:size(H,2);
                            holder(i,:)=holder(i,:)+H{j,i};
                        end
                        for i=1:size(SHIFT,2);
                            holderSHIFT(i,:)=holderSHIFT(i,:)+SHIFT{j,i};
                        end
                        rate1=mean(d{site,1}{list(pair,1)+1,1}.([files{filetype}]).spikes_count(j,:));
                        rate2=mean(d{site,1}{list(pair,2)+1,1}.([files{filetype}]).spikes_count(j,:));
                        SmCCG=(smooth(sum(holder,1))/sqrt(rate1*rate2))/199;
                        ShiftCCG=(smooth(sum(holderSHIFT,1))/sqrt(rate1*rate2))/199;
                        SmCCGCorrected=SmCCG-ShiftCCG;
                        MAX=max(SmCCGCorrected);
                        
                        lag=find(SmCCGCorrected==max(SmCCGCorrected));
                        if isempty(lag) || sum(isnan(lag))>0 || MAX==0;
                            lag=100;
                        end
                        if length(lag)>1;
                            tempMAX=lag;
                            tempMAX=abs(tempMAX-100);
                            selection=find(tempMAX==min(tempMAX),1,'first');
                            lag=lag(selection);
                        end

                        if lag==199;
                            tlag=198;
                        else
                            tlag=lag;
                        end

                        positive=(SmCCGCorrected>=(MAX/2));
                        pos=diff(positive);
                        less=pos(1:tlag);
                        more=pos(tlag:end);
                        Low=find(less==1,1,'last');
                        Low=length(less)-Low;
                        High=find(more==-1,1,'first');
                        bandMAX=High+Low;

                        PEAK=SmCCGCorrected(100,1);

                        positive=(SmCCGCorrected>=(PEAK/2));
                        pos=diff(positive);
                        less=pos(1:100);
                        more=pos(100:end);
                        Low=find(less==1,1,'last');
                        High=find(more==-1,1,'first');
                        Low=length(less)-Low;
                        bandPEAK=High+Low;

                        if isempty(bandMAX);
                            bandMAX=0;
                        end
                        if isempty(bandPEAK);
                            bandPEAK=0;
                        end

                        negative=SmCCGCorrected<0;
                        auc=SmCCGCorrected;
                        auc(negative)=0;
                        
                        AUC=sum(auc);
                        S=std(holder,0,1);
                        flankS=std([SmCCGCorrected(1:25);SmCCGCorrected(end-25:end)]);
                        flankM=mean([SmCCGCorrected(1:25);SmCCGCorrected(end-25:end)]);

                        if isnan(AUC)
                            check=0;
                        elseif sum(sum(holder,1)) < 2;
                            check = 0;
                        elseif max(SmCCGCorrected)<flankM+(5*flankS);
                            check = 0;
                        else
                            check=1;
                        end
                        
                        TempCorr(j,1)=date;
                        TempCorr(j,2)=site;
                        TempCorr(j,3)=list(pair,1);
                        TempCorr(j,4)=list(pair,2);
                        TempCorr(j,5)=AUC;      % Area under the whole curve
                        TempCorr(j,6)=MAX;      % Maximum coincidences
                        TempCorr(j,7)=lag;      % millisecond the maximum occurs at
                        TempCorr(j,8)=bandMAX;  % bandwidth surrounding the max
                        TempCorr(j,9)=PEAK;     % Coincidences at the center
                        TempCorr(j,10)=bandPEAK; % bandwidth surrounding the center
                        TempCorr(j,11)=sum(S);   % Standard deviation
                        TempCorr(j,12)=check;    % Pass criteria
                        
                        SmCCGCorrectedS=SmCCGCorrectedS+SmCCGCorrected;
                        SmCCGS=SmCCGS+SmCCG;
                        ShiftCCGS=ShiftCCGS+ShiftCCG;
                        
                    end
                    
%                     plot(SmCCGCorrectedS,'k');
%                     hold on
%                     plot(SmCCGS,'--b');
%                     plot(ShiftCCGS,'--r');
%                     title(['Date = ' num2str(date) '; Site = ' num2str(site) '; File = ' num2str(filetype) '; Pair = ' num2str(pair) '; Line = ' num2str(j)]);
%                     hold off
%                     pause
                    Pdata{1}=[Pdata{1}; date,site,filetype,pair,SmCCGCorrectedS'];
                    Pdata{2}=[Pdata{2}; date,site,filetype,pair,SmCCGS'];
                    Pdata{3}=[Pdata{3}; date,site,filetype,pair,ShiftCCGS'];
                    filefound=0;
                    for quickcheck=1:size(data,1);
                        if TempCorr(1,3)==data{quickcheck,1}(1,1) && TempCorr(1,4)==data{quickcheck,1}(1,2);
                            data{quickcheck,1}=([data{quickcheck,1}; TempCorr]);
                            filefound=filefound+1;
                        end
                    end
                    if isempty(quickcheck);
                        quickcheck=0;
                    end
                    if filefound==0;
                        data{quickcheck+1,1}=TempCorr;
                    end
                end        % END for Pairs
            end                % END for check if is field

        end                    % END for filetypes
        for compiler = 1:size(data,1);
            x = data{compiler}(:,3)==0;
            data{compiler}(x,:)=[];
            x = isnan(data{compiler}(:,3));
            data{compiler}(x,:)=[];
            x = data{compiler}(:,12)==0;
            data{compiler}(x,:)=[];
            peak=data{compiler}(:,9:10);
            x=peak(:,1)==0;
            peak(x,:)=[];
            OUTPUT(OutputCounter,:)=nanmean(data{compiler},1);
            OUTPUT(OutputCounter,9:10)=nanmean(peak,1);
            OutputCounter=OutputCounter+1;
        end
    end                        % END for SITE
end                            % END for date
end                            % END for function

