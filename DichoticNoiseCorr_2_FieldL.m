addpath('C:\Users\mbeck\Desktop\Code\Matlab Offline Files SDK')

Chan = 1;
Event = 1;
count = 1;
StartTime= 0.0;
Window= 0.55;
Base= 0.1;

cd('C:\Users\mbeck\Desktop\Data\FieldL\frozen\')
files = ls;

data = cell(size(files,1)-2,1);

for f = 3:length(files)
    unit = plx_info(files(f,:),0);
    unit = unit(:,2);
    x = find(unit == 0,1,'first');
    unit = x-1;
    
    DATA = cell(unit,1);
    
    for U = 1:unit
        
        [~,SpikeTimes]=plx_ts(files(f,:),Chan,U);
        [~,EventTimes]=plx_event_ts(files(f,:),Event);
        EventTimes=unique(EventTimes);
        DiffEvent=diff(EventTimes);
        MeanISI=mean(DiffEvent);
        [r]=find(DiffEvent<MeanISI/2);
        EventTimes(r+1)=[];
        clear MeanISI DiffEvent r tyto_str plx_str
        
        depvar = [1 length(EventTimes)];
        
        % Set up holders for the data
        base_times=cell(depvar);
        spikes_times=cell(depvar);
        spikes_count=zeros(depvar);
        
        
        for event_counting = 1:length(EventTimes)
            sp=find((SpikeTimes>=EventTimes(event_counting,1)+StartTime) & (SpikeTimes<=EventTimes(event_counting,1)+StartTime+Window));
            ba=find((SpikeTimes>=EventTimes(event_counting,1)-Base+StartTime) & (SpikeTimes<=EventTimes(event_counting,1)));
            base_times{event_counting}=SpikeTimes(ba)-EventTimes(event_counting,1); %#ok<FNDSB>
            spikes_times{event_counting}=SpikeTimes(sp)-EventTimes(event_counting,1); %#ok<FNDSB>
            spikes_count(event_counting)=sum((SpikeTimes>=EventTimes(event_counting,1)+StartTime) & (SpikeTimes<=EventTimes(event_counting,1)+Window));
        end
        
        for reps=1:size(depvar,2)
            [uniDV,ind]=sortrows(depvar(:,reps));
            base_times(:,reps)=base_times(ind,reps);
            spikes_times(:,reps)=spikes_times(ind,reps);
            spikes_count(:,reps)=spikes_count(ind,reps);
        end
        
        DATA{U,1}.spikes_times=spikes_times;
        DATA{U,1}.base_times=base_times;
        DATA{U,1}.spikes_count=spikes_count;
        DATA{U,1}.depvar=unique(depvar(:,1));
        DATA{U,1}.reps=size(depvar,2);
        
    end
    
    data{f-2} = DATA;
    
end

clearvars -except data

data = data(~cellfun(@isempty,data));

%%

% del = 0.1;
window = [0.1, 0.15, 0.2, 0.3, 0.4, 0.5];
% window = window+del;
out = cell(length(window),1);


for w = 1:length(window)

out{w} = nan(1000,1);

for site = 1:length(data)
    list = combnk(1:length(data{site}),2);
    for pair = 1:size(list,1)
        unit1 = data{site}{list(pair,1)}.spikes_times;
        unit2 = data{site}{list(pair,2)}.spikes_times;
        
        check = cellfun(@(x) x<window(w), unit1, 'UniformOutput', false);
        for i = 1:length(check)
            unit1{i}=unit1{i}(check{i});
        end
        check = cellfun(@(x) x<window(w), unit2, 'UniformOutput', false);
        for i = 1:length(check)
            unit2{i}=unit2{i}(check{i});
        end
        clear i check
        
        unit1 = cellfun(@length,unit1);
        unit2 = cellfun(@length,unit2);
        
        c = corr(unit1',unit2');
        L = find(isnan(out{w}),1,'first');
        out{w}(L) = 0.5*log((1+c)/(1-c));
    end
end

L = find(isnan(out{w}),1,'first');
out{w}(L:end)=[];

end

clearvars -except data out