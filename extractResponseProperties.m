%% load data
load('C:\Users\mbeck\Desktop\Data\FreeField\LargeWindowAnalysis\FreeFieldRawDATA_LWA.mat')

%% Extract the SpRF and PSTHs and Spiketimes for rasters

for R = 1:3
    
    switch R
        case 1
            data = OT;
        case 2
            data = FL;
        case 3
            data = AAr;
    end
    
    output = cell(1,150);
    outs = cell(1,150);
    his = cell(1,150);
    ras = cell(1,150);
    
    for date = 1:length(data)
        for site = 1:length(data{date})
            for unit = 2:length(data{date}{site})
                d = data{date}{site}{unit};
                files = fieldnames(d);
                idx = zeros(length(files),1);
                for c = 1:length(files)
                    if length(files{c})>2
                        idx(c)=strcmp(files{c}(1:2),'FF');
                    end
                end
                idx=logical(idx);
                files=files(idx);
                holder = cell(1,length(files));
                for f = 1:length(files)
                    holder{f} = d.(files{f}).spikes_count;
                end
                holder=cell2mat(holder);
                idx = find(cellfun(@isempty,output),1,'first');
                output{idx}=mean(holder,2);
                outs{idx}=std(holder,0,2)/sqrt(size(holder,2));
                
                h = zeros(1,71);
                r = [];
                best=find(output{idx}==max(output{idx}),1,'last');
                for f = 1:length(files)
                    c = d.(files{f}).spikes_times;
                    for t = 1:size(c,2)
                        h=h+hist(c{best,t},-0.05:0.005:0.3);
                    end
                    r=[r,c(best,:)];
                end
                his{idx}=h';
                ras{idx}=r;
                
                clear d files holder f idx c h best t r
            end
        end
    end
    idx = find(cellfun(@isempty,output),1,'first');
    output=cell2mat(output(1:idx-1));
    outs=cell2mat(outs(1:idx-1));
    his=cell2mat(his(1:idx-1));
    ras=ras(1:idx-1);
    
    Pdata = output/0.35;
    Ps = outs/0.35;
    
    clear date site idx unit data output outs
    
    switch R
        case 1
            ot.curve = Pdata;
            ot.sem = Ps;
            ot.his = his;
            ot.ras = ras;
        case 2
            fl.curve = Pdata;
            fl.sem = Ps;
            fl.his = his;
            fl.ras = ras;
        case 3
            aar.curve = Pdata;
            aar.sem = Ps;
            aar.his = his;
            aar.ras = ras;
    end
    
    clear Pdata Ps his ras
    
end

clear R

%% Transform SpRF to Azimuth curves

load('C:\Users\mbeck\Desktop\Data\Analysis\SpeakerIndex.mat')
[B,I]=sortrows(SpeakerIndex(:,1));
H=hist(B,-100:10:100);
H=cumsum(H);
clear B

for r = 1:3
    switch r
        case 1
            d = ot.curve;
            s = ot.sem;
        case 2
            d = fl.curve;
            s = fl.sem;
        case 3
            d = aar.curve;
            s = aar.sem;
    end
    
    out = nan(21,size(d,2));
    outs = nan(21,size(d,2));
    
    for UNIT=1:size(d,2)
        unit=d(:,UNIT);
        unitA=unit(I);
        unit=s(:,UNIT);
        unitS=unit(I);
        Az=zeros(21,1);
        Az(1)=mean(unitA(H(1)),2);
        for point=2:length(H)
            Az(point)=mean(unitA(H(point-1):H(point)));
        end
        S=zeros(21,1);
        S(1)=mean(unitS(H(1)),2);
        for point=2:length(H)
            S(point)=mean(unitS(H(point-1):H(point)));
        end
        
        out(:,UNIT)=Az;
        outs(:,UNIT)=S;
        
    end
    
    switch r
        case 1
            ot.AzM = out;
            ot.AzS = outs;
        case 2
            fl.AzM = out;
            fl.AzS = outs;
        case 3
            aar.AzM = out;
            aar.AzS = outs;
    end
    
end

clearvars -except aar ot fl

%% save the data from above
cd('C:\Users\mbeck\Desktop\Data\FreeField\LargeWindowAnalysis\')
save FreeField_ResponseProperties_LWA

%% Load data that are saved from above
load('C:\Users\mbeck\Desktop\Data\FreeField\LargeWindowAnalysis\FreeField_ResponseProperties_LWA.mat')

%% Load parameters for plotting
load('C:\Users\mbeck\Desktop\Data\Analysis\SpeakerIndex.mat')
load('C:\Users\mbeck\Desktop\Data\Analysis\MyColorMaps.mat')

%% Plot the SpRFs and request a classification

% key = nan(size(Pdata,2),1);
% i=1;
% while i<=size(Pdata,2)
% SpRF = [SpeakerIndex,Pdata(:,i)];
% contourplot2(SpRF, X, Y, 15);
% fig=gcf;
% title(num2str(i));
% xlabel('Speaker Azimuth (deg)');
% ylabel('Speaker Elevation (deg)');
% zlabel('# spikes');
% colorbar
% set(fig,'Colormap',soft);
% check = input('class: 1-single, 2-hemifield, 3-streak, 4-multi, 5-sparse, 6-messy, {0}=goback');
% if isempty(check) || check == 0
%     i = i-1;
% else
%     key(i)=check;
%     i = i+1;
% end
%
% end
%
% clear SpRF i

%% Plot the everything for observations

for R = 1:3
    switch R
        case 1
            data = ot;
            ex = 32;
        case 2
            data = fl;
            ex = [1,2,3,16];
        case 3
            data = aar;
            ex = 17;
    end
    
x = -100:10:100;
X=-100:2.5:100; Y=-80:2.5:80;

% for i = 1:size(data.curve,2)
for i = ex
    
%     subplot(3,2,1)
    subplot(3,2,1)
    SpRF = [SpeakerIndex,data.curve(:,i)];
    contourplot2(SpRF, X, Y, 15);
    fig=gcf;
    title(['region = ' num2str(R)]);
    xlabel('Speaker Azimuth (deg)');
    ylabel('Speaker Elevation (deg)');
    zlabel('# spikes');
    colorbar
    set(fig,'Colormap',soft);
    axis square
    
%     subplot(3,2,2)
    subplot(4,1,2)
    errorbar(x,data.AzM(:,i),data.AzS(:,i))
    title(['Unit = ' num2str(i)]);
    xlim([-100 100])
    xlabel('Speaker Azimuth (deg)');
    ylabel('Firing Rate (Hz)');
    axis square
    
%     subplot(3,2,[3,4])
    subplot(4,1,3)
    bar(-0.05:0.005:0.3,data.his(:,i))
    xlim([0 0.3])
    xticklabels({'-50','0','50','100','150','200','250'})
    xticks([0 0.05 0.1 0.15 0.2 0.25 0.3])
    xlabel('Time (ms)');
    ylabel('Spike Count');
    
%     subplot(3,2,[5,6])
    subplot(4,1,4)
    r = data.ras{i};
    l = cellfun(@length,data.ras{i});
    m = max(l);
    times = cell(1,size(r,2));
    for k = 1:size(r,2)
        times{k} = ones(l(k),1)*k;
        r{k}=[r{k}; nan(m-length(r{k}),1)]';
        times{k}=[times{k}; nan(m-length(times{k}),1)]';
    end
    times = cell2mat(times); y = cell2mat(r);
    scatter(y,times,5,'fill','k')
    xlim([0 0.3])
    xticklabels({'-50','0','50','100','150','200','250'})
    xticks([0 0.05 0.1 0.15 0.2 0.25 0.3])
    xlabel('Time (ms)');
    ylabel('Firing Rate (Hz)');
    
    pause
    
end

end
% clearvars -except aar ot fl
