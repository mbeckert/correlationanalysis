% requires the analysis of the data set 'FreeFieldRAWdata_notsidecorrected'
% using either 'Analysis_az_FULL' or 'Analysis_itd_FULL'
% before use. Save the output and load them here, specifically needs the
% 'distant','nearby', and 'SIG_d_A' variables

% set to the root where the files are saved
root='C:\Users\mbeck\Desktop\Data\Analysis\CorrelationsPaper\';

% select which data set to load
% file='SignalCorrelationITD_Updated.mat';
file='SignalCorrelationAz_Updated.mat';

load([root file],'nearby','distant','SIG_d_A');

clear root file

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot data into boxplots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FishZ=@(x) 0.5*log((1+x)./(1-x));
invF=@(x)(exp(2.*x)-1)./(1+exp(2.*x));

t={'Nearby','Distant','AAr'};
labels={'Full','Ipsilateral','Frontal','Contralateral'};
regions = {'OT','FL','AAr'};
for f = 1:3

    switch f
        case 1
            Y=cell(1,3);
            Y{1}=invF(nearby(:,1));
            Y{2}=invF(nearby(:,2));
            Y{3}=invF(nearby(:,3));
            X=2:2:6;
            r=regions;
            ttl=['SigCorr - ' t{f}];
        case 2
            Y=cell(1,2);
            Y{1}=invF(distant(:,1));
            Y{2}=invF(distant(:,2));
%             Y{1}=distant(:,1);
%             Y{2}=distant(:,2);
            X=2:2:4;
            r=regions(2:3);
            ttl=['SigCorr - ' t{f}];
        case 3
            Y=cellfun(invF,SIG_d_A,'UniformOutput',0);
            X=2:2:8;
            r=labels;
            ttl=regions{3};
    end
    
    figure(f)
    boxplot2_points(X,Y)
    % some settings for the figure
    set(gca,'xtick',X,'xticklabel',r,'ytick',-1:0.5:1)
    title(ttl)
    box off
    xlim([0 X(end)+2])
%     ylim([-1 1])
    
end

clear X Y labels r d regions FishZ invF r t f ttl
%%

invF=@(x)(exp(2.*x)-1)./(1+exp(2.*x));

x = invF(nearby);
N = [nanmean(x);nanstd(x)];
x = invF(distant);
D = [nanmean(x);nanstd(x)];

x = cellfun(invF,SIG_d_A,'UniformOutput',0);
A = [cellfun(@nanmean,x);cellfun(@nanstd,x)];



%%

i=idx_aar;
n=SIG_n_A;
dis=SIG_d_A;

count=[0,0,0];
count(2)=sum(~isnan(n));
count(3)=sum(~isnan(dis{1}));

for d=1:max(i(:,1))
    for s=1:max(i(:,2))
        x1=find(i(:,1)==d);
        x2=find(i(:,2)==s);
        x=intersect(x1,x2);
        if ~isempty(x)
            count(1)=count(1)+1;
        end
    end
end

clear i d s x* n dis