function [DATA,KEY,STD]=CorrelationAnalysisPopulation_Omitted

% AAr
FILES = {'data_006_2015_0502','data_006_2015_0521','data_006_2015_0616','data_006_2015_0713','data_006_2015_0722', ...
    'data_021_2015_0528','data_021_2015_0609','data_021_2015_0706','data_021_2015_0721','data_021_2015_0730', ...
    'data_023_2015_0422','data_023_2015_0501','data_023_2015_0610', ...
    'data_029_2015_0910','data_029_2015_0929', ...
    'data_032_2015_0903'};
Right = [6,7,8,10];

% OT
% FILES = {'data_029_2015_1029','data_029_2015_1112','data_029_2015_1123','data_029_2015_1214', ...
%     'data_032_2015_1110','data_032_2015_1119','data_032_2016_0223'};
% Right = [3,4,6];

% Field L
% FILES = {'data_029_2016_0113','data_029_2016_0510', ...
%     'data_032_2016_0106','data_032_2016_0119','data_032_2016_0202','data_032_2016_0503','data_032_2016_0517'};
% Right = 2;

%% Set Up

files.abi={'abi'};
files.bf={'bf','bf000','bf060','bf120','bf180','bf1','bf2'};
files.ild={'ild','ILD'};
files.itd={'itd','ITD',     'ILDp40','ILDp35','ILDp30','ILDp25','ILDp20','ILDp15', 'ILDp10','ILDp05', 'ILD000', ...
    'ILDn05','ILDn10','ILDn15','ILDn20','ILDn25','ILDn30','ILDn35','ILDn40'};
files.FF={'FF1','FF2','FF3','FF4','FF5','FFf1','FFf2','FFf3','FFf4'};



type={'abi','bf','ild','itd','FF'};

% OUTPUT=[];
DATA=[];
KEY=[];

FFflip=[3,2,1,8,7,6,5,4,15,14,13,12,11,10,9,24,23,22,21,20,19,18,17,16,31,30,29,28,27,26,25,40,39,38,37,36,35,34,33,32,51,50,49,48,47,46,45,44,43,42,41,62,61,60,59,58,57,56,55,54,53,52,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,64,63,94,93,92,91,90,89,88,87,86,85,84,105,104,103,102,101,100,99,98,97,96,95,114,113,112,111,110,109,108,107,106,121,120,119,118,117,116,115,130,129,128,127,126,125,124,123,122,137,136,135,134,133,132,131,142,141,140,139,138,144,143];


for date = 1:length(FILES);
    if sum(Right==date);
        correction=1;
    else
        correction=0;
    end
    disp(['Loading File ' FILES{date}]);
    load([FILES{date} '.mat']);
    d= eval(FILES{date});
    eval(['clear ' FILES{date}]);

    for TYPE=1:length(type);
        for site = 1:length(d);
            for unit = 2:length(d{site,1});
                x=[];
                sensitive=0;
%                 used=0;
                for filetype=1:length(files.(type{TYPE}));
%                     if used<=0;     % Edited so we only collect the first file available for each file type. This should eliminate any adaptation or anesthesia effects
                    if isfield(d{site,1}{unit,1},files.(type{TYPE}){filetype});
                        % Change this line to be "spikes_count" for evoked
                        % responses and "base_count" for baseline spikes
                        % evoked_z_count
                        x=[x,d{site,1}{unit,1}.([files.(type{TYPE}){filetype}]).spikes_count];
                        sensitive=sensitive+sum(d{site,1}{unit,1}.([files.(type{TYPE}){filetype}]).sensitive);
                    end
%                     end
                end             % end for filetype

                if ~isempty(x);
                    x(isnan(x))=0;
                    
                    y=nanmean(x,2)';
                    z=nanstd(x,1,2)';
                    if isnan(y);
                        y=[];
                        z=[];
                    end
                    if size(y,2)==48;
                        y=[];
                        z=[];
                    end
                    if size(y,2)==17;
                        y=[0,0,y,0,0];
                        z=[0,0,z,0,0];
                    end

                    % This flips the vectors to correct for the recording
                    % hemisphere
                    if ~isempty(y);
                        if TYPE>=3 && correction==1;
                            if TYPE==5;
                                y=y(FFflip);
                                z=z(FFflip);
                            else
                                y=fliplr(y);
                                z=fliplr(z);
                            end
                        end
                    end
                    if sensitive >0;
                        SENSE = 1;
                    else
                        SENSE=0;
                    end
                    if isempty(y);
                        y=nan(1,size(DATA.([type{TYPE}]),2));
                        z=nan(1,size(DATA.([type{TYPE}]),2));
                    end
%                     key=[date,site,location.A(site),location.L(site),location.DV(site),unit,SENSE];
                        key=[date,site,unit,SENSE,size(x,2)];
                    if isfield(DATA,type{TYPE});
                        DATA.([type{TYPE}])=[DATA.([type{TYPE}]);y];
                        KEY.([type{TYPE}])=[KEY.([type{TYPE}]);key];
                        STD.([type{TYPE}])=[STD.([type{TYPE}]);z];
                    else
                        DATA.([type{TYPE}])=y;
                        KEY.([type{TYPE}])=key;
                        STD.([type{TYPE}])=z;
                    end
                    clear key y x z
                end
            end         % end for unit

        end             % end for site
    end                 % end for TYPE
end                     % end for date

% This section now clears all units that are insensitive according to the 2
% standard deviations above baseline criterion. Only those that are
% sensitive are used for the correlation analysis.

% files=fields(DATA);
% for filetype=1:length(files);
%     for unit=1:size(KEY.([files{filetype}]));
%         tempkey=KEY.([files{filetype}]);
%         tempdata=DATA.([files{filetype}]);
%         tempdata(tempkey(:,4)==0,:)=[];
%         tempkey(tempkey(:,4)==0,:)=[];
%         [a b]=find(tempkey(:,1)==tempkey(1,1)); %#ok<NASGU>
%         [c d]=find(tempkey(:,2)==tempkey(1,2)); %#ok<NASGU>
%         same=intersect(a,c);
%         clear a b c d
%         same(same==unit)=[];
%         tempdata(same,:)=[];
%     list=combnk(1:size(tempdata,1),2);
%     OUTPUT.([files{filetype}])=[];
%     for i=1:size(list,1);
%         CORR=corrcoef(tempdata(list(i,1),:),tempdata(list(i,2),:),'rows','complete');
%         OUTPUT.([files{filetype}])=[OUTPUT.([files{filetype}]);CORR(1,2)];
%     end
%     OUTPUT.([files{filetype}])(isnan(OUTPUT.([files{filetype}])))=[];
%     clear tempkey tempdata
%     end
% end