%%
load('C:\Users\PenaLab\Desktop\FreeField\Analysis\Data\CURVES.mat')
%%

band=cell(1,3);

for r=1:3;
    if r == 1; d=CURVE_OT.itd; end %d(50,:)=[]; end % If you are running Az, d(50,:)=[];
    if r == 2; d=CURVE_FL.itd; end %d(73,:)=[]; end % If you are running Az, d(73,:)=[];
    if r == 3; d=CURVE_AAr.itd; end
    
band{r}=nan(size(d,1),1);

for i = 1:size(d,1)
    c=smooth(d(i,:))';
    c=norm01(c,1);
    best=find(c==max(c),1,'last');
    
    % use this to find the halfwidth
%     response=c>=c(best)/2;

    % use this to find the bandwidth of the whole curve
    base=quantile(c,0.1);
    response=c>=base;
    
    % use for all from here on
    change = diff(response);
    
    pos = find(change == 1);
    neg = find(change == -1);
    if isempty(pos)
        pos=1;
    end
    if isempty(neg)
        neg=21;
    end
    
    if pos(1)>best
        pos=[1 pos];
    end
    if neg(end) < best
        neg=[neg 21];
    end
    
    pos=best-pos;
    neg=neg-best;
    
    neg(neg<0)=[];
    pos(pos<0)=[];
    
    band{r}(i)=min(neg)+min(pos);
    
end
    
    clear i c best response change pos neg base
    
end
    
    clear r d