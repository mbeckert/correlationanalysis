% Use "TempCorrFR" to generate a Synchrony data file. Load that here to
% extract the desired parameters for stats and plotting.

%%
load('C:\Users\mbeck\Desktop\Data\Analysis\CorrelationsPaper\SynchRAWdata.mat');
%%
for b = 1
    for s = 1:2
        switch s
            case 1
                d = SynchBase(:,b);
            case 2
                d = SynchEvoked(:,b);
        end
        
        out = cell(1,3);
        KEY = cell(1,3);
        
        
        for R = 1:3
            out{R} = nan(500,4);
            KEY{R} = nan(500,4);
            filenames = fieldnames(d{R});
            mdate = 0; msite = 0; mu1 = 0; mu2 = 0;
            for file = 1:length(filenames)
                tmpdate = [mdate max(d{R}.(filenames{file}){1}(:,1))];
                tmpsite = [msite max(d{R}.(filenames{file}){1}(:,2))];
                tmpu1 = [mu1 max(d{R}.(filenames{file}){1}(:,3))];
                tmpu2 = [mu2 max(d{R}.(filenames{file}){1}(:,4))];
                mdate = max(tmpdate);
                msite = max(tmpsite);
                mu1 = max(tmpu2);
                mu2 = max(tmpu2);
            end
            
            clear tmp*
            for date = 1:mdate
                for site = 1:msite
                    for unit1 = 1:mu1
                        for unit2 = 1:mu2
                            check = nan(100,50);
                            peak = nan(100,50);
                            band = nan(100,50);
                            sumband = nan(100,50);
                            auc = nan(100,50);
                            for file = 1:length(filenames)
                                key = d{R}.(filenames{file}){1};
                                i1 = find(key(:,1) == date);
                                i2 = find(key(:,2) == site);
                                i3 = find(key(:,3) == unit1);
                                i4 = find(key(:,4) == unit2);
                                idx = intersect(i1,(intersect(i2,(intersect(i3,i4)))));
                                if ~isempty(idx)
                                    L = find(isnan(check(:,1)),1,'first');
                                    c = length(d{R}.(filenames{file}){2}(idx,:));
                                    check(L,1:c) = 1;
                                    peak(L,1:c) = d{R}.(filenames{file}){2}(idx,:);
                                    band(L,1:c) = d{R}.(filenames{file}){6}(idx,:);
                                    sumband(L,1:c) = d{R}.(filenames{file}){8}(idx,:);
                                    auc(L,1:c) = d{R}.(filenames{file}){7}(idx,:);
                                end
                            end         % FILE
                            if ~isempty(check)
                                L = find(isnan(out{R}(:,1)),1,'first');
                                out{R}(L,1) = nanmean(peak(:));
                                out{R}(L,2) = nanmean(band(:)); % change this to what metric you want to go with
                                out{R}(L,3) = nanmean(sumband(:));
                                out{R}(L,4) = nanmean(auc(:));
                                KEY{R}(L,:) = [date,site,unit1,unit2];
                                disp(['Bin = ' num2str(b) '; stim = ' num2str(s) '; R = ' num2str(R) '; D = ' num2str(date) '; S = ' num2str(site) '; U1 = ' num2str(unit1) '; U2 = ' num2str(unit2)])
                            end         % EMPTY
                        end             % UNIT2
                    end                 % UNIT1
                end                     % SITE
            end                         % DATE
            
            out{R}(L:end,:)=[];
            KEY{R}(L:end,:)=[];
            
        end                             % R
        
        switch s
            case 1
                B = [KEY;out];
            case 2
                E = [KEY;out];
        end
        
    end
    
    switch b
        case 1
            S01 = [B,E];
        case 2
            S02 = [B,E];
        case 3
            S05 = [B,E];
        case 4
            S10 = [B,E];
        case 5
            S20 = [B,E];
    end
end

clearvars -except S0* S10 S20 Synch*


%%

d = S01;
d = d(2,:);

m = max(cellfun(@length,d));
for i = 1:length(d)
    d{i}=[d{i};nan(m-length(d{i}),4)];
end

out = cell2mat(d);

for j = 1:4
    
    switch j
        case 1
            x = 1:4:21;
            peak = out(:,x);
        case 2
            x = 2:4:22;
            band = out(:,x);
        case 3
            x = 3:4:23;
            sumband = out(:,x);
        case 4
            x = 4:4:24;
            auc = out(:,x);
    end
    
end

clear data d m i j out x

%%

