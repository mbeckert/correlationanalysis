function [OUTPUT]=CorrelationAnalysisFFf

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AAr
FILES = {'data_006_2015_0502','data_006_2015_0521','data_006_2015_0616','data_006_2015_0713','data_006_2015_0722', ...
    'data_021_2015_0528','data_021_2015_0609','data_021_2015_0706','data_021_2015_0721','data_021_2015_0730', ...
    'data_023_2015_0422','data_023_2015_0501','data_023_2015_0610', ...
    'data_029_2015_0910','data_029_2015_0929', ...
    'data_032_2015_0903'};

% OT
% FILES = {'data_029_2015_1029','data_029_2015_1112','data_029_2015_1123','data_029_2015_1214', ...
%     'data_032_2015_1110','data_032_2015_1119','data_032_2016_0223'};

% Field L
% FILES = {'data_029_2016_0113','data_029_2016_0510', ...
%     'data_032_2016_0106','data_032_2016_0119','data_032_2016_0202','data_032_2016_0503','data_032_2016_0517'};


OUTPUT = cell(length(FILES),2);

for date = 1:length(FILES);

    disp(['Loading File ' FILES{date}]);
    load([FILES{date} '.mat']);
    eval(['d=' FILES{date} ';']);
    eval(['clear ' FILES{date}]);

    % Create the analysis structure that the correlation data will be put into
    % later
    ANALYSIS=cell([length(d),1]);
    for site=1:length(d);

        list=combnk(1:size(d{site,1},1)-1,2);
        analysis=cell(size(list,1),1);
        for g=1:size(list,1);
            analysis{g,1}=cell(1,4);
            analysis{g,1}{3}=cell(1,2);
            analysis{g,1}{1}=list(g,:);
        end

        % For signal correlation of pairs
        for FF=1:2;
            disp('Running Signal Correlation');
            checker=0;

            for h=1:size(d{site,1},1)-1;
                if isfield(d{site,1}{h+1,1},(['FFf' num2str(FF)]));
                    checker=checker+1;
                end
            end
            if checker>=1;
                list=1:size(d{site,1},1)-1;
                for h=1:size(d{site,1},1)-1;
                    if ~isfield(d{site,1}{h+1,1},['FFf' num2str(FF)])
                        list(h)=0;
                    end
                end
                list(list==0)=[];
                list=combnk(list,2);

                NoiseCorr=zeros([1 2]);

                pair = 0;
                while pair < size(list,1);
                    pair = pair+1;
                    M1=nanmean(d{site,1}{list(pair,1)+1,1}.(['FFf' num2str(FF)]).spikes_count,2);
                    M2=nanmean(d{site,1}{list(pair,2)+1,1}.(['FFf' num2str(FF)]).spikes_count,2);
                    BEST=zeros([1 2]);
                    r1=find(M1==max(M1));
                    r2=find(M2==max(M2));
                    BEST(1)=d{site,1}{list(pair,2)+1,1}.(['FFf' num2str(FF)]).depvar(r1(1));
                    BEST(2)=d{site,1}{list(pair,2)+1,1}.(['FFf' num2str(FF)]).depvar(r2(1));
                    CORR=corrcoef(M1,M2,'rows','complete');
                    scatter(M1,M2,'jitter','on','jitterAmount',0.1);
                    if CORR == 1;
                        CORR(1,2) = 1;
                    elseif isnan(CORR);
                        CORR(1,2) = NaN;
                    end
                    check=input(['Pair ' num2str(pair) ' look okay? ({1=yes}, 0=no, 2=go back) Corr = ' num2str(CORR(1,2)) ' = ']);
                    if isempty(check);
                        check=1;        % Makes "yes" the default response
                    elseif check == 2;
                        pair=last-1;
                    end
                    last=pair;
                    NoiseCorr(1,2)=check;
                    NoiseCorr(1,1)=CORR(1,2);
                    for i=1:size(analysis,1);
                        if analysis{i,1}{1}==list(pair,:);
                            analysis{i,1}{2}{FF}=NoiseCorr;
                            analysis{i,1}{4}{FF}=BEST;
                        end
                    end
                end
            end
        end

        %   For FF frozen noise correlation
        for pair=1:size(list,1);
            for FF=1:2;
                disp('Running Noise Correlation');
                checker=0;
                if isfield(d{site,1}{list(pair,1)+1,1},(['FFf' num2str(FF)]));
                    checker=checker+1;
                    if isfield(d{site,1}{list(pair,2)+1,1},(['FFf' num2str(FF)]));
                        checker=checker+1;
                    end
                end
                if checker==2;
                    STEP=size(d{site,1}{list(pair,1)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count,2)/5;
                    %             list=1:size(d{site,1},1)-1;
                    %             for h=1:size(d{site,1})-1;
                    %                 if ~isfield(d{site,1}{h+1,1},['FFf' num2str(FF)])
                    %                     list(h)=0;
                    %                 end
                    %             end
                    %             list(list==0)=[];
                    %             list=combnk(list,2);
                    NoiseCorr=cell(1,4);
                    NoiseCorr{1}=zeros([144 2]);
                    NoiseCorr{2}=zeros([144 2]);
                    NoiseCorr{3}=zeros([144 2]);
                    NoiseCorr{4}=zeros([144 2]);
                    for int=1:STEP;
                        speakernum=1;
                        while speakernum<144;
                            if speakernum == 1;
                                last = 1;
                            end
                            speakernum=speakernum+1;
                            if isnan(sum(d{site,1}{list(pair,1)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5))) || isnan(sum(d{site,1}{list(pair,2)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5)))
                                NoiseCorr{int}(speakernum,1:2)=[NaN NaN];
                                disp(['Speaker Number ' num2str(speakernum) ' Non-responsive pair']);
                            else
                                CORR=corrcoef(d{site,1}{list(pair,1)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5),d{site,1}{list(pair,2)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5));
                                                            scatter(d{site,1}{list(pair,1)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5),d{site,1}{list(pair,2)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5),'jitter','on','jitterAmount',0.1);
                                pause
                                                            %                             check=input(['Speaker Number ' num2str(speakernum) ' look okay? ({1=yes}, 0=no, 2=go back) Corr = ' num2str(CORR(1,2)) ' = ']);
                                %                             if isempty(check);
                                %                                 check=1;        % Makes "yes" the default response
                                %                             elseif check == 2;
                                %                                 speakernum=last-1;
                                %                             end
                                %                             last=speakernum;
                                if isnan(CORR(1,2));
                                    disp(['Speaker Number ' num2str(speakernum) ' does not have range']);
                                    check = 0;
                                else
                                    if length(unique(d{site,1}{list(pair,1)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5)))>2 && length(unique(d{site,1}{list(pair,2)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5)))>2;
                                        disp(['Speaker Number ' num2str(speakernum) ' Sufficient Range']);
                                        check = 1;
                                    elseif sum(d{site,1}{list(pair,1)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5)==max(d{site,1}{list(pair,1)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5)))>1 || sum(d{site,1}{list(pair,2)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5)==max(d{site,1}{list(pair,2)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5)))>1;
                                        disp(['Speaker Number ' num2str(speakernum) ' Sufficient Response Trials']);
                                        check = 1;
                                    else
                                        disp(['Speaker Number ' num2str(speakernum) ' Single Response Trial']);
                                        check = 0;
                                    end
                                end
                                NoiseCorr{int}(speakernum,2)=check;
                                NoiseCorr{int}(speakernum,1)=CORR(1,2);
                                NoiseCorr{int}(speakernum,3)=nanmean(d{site,1}{list(pair,1)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5));
                                NoiseCorr{int}(speakernum,4)=nanmean(d{site,1}{list(pair,2)+1,1}.(['FFf' num2str(FF)]).omitted_spikes_count(speakernum,1:int*5));
                            end
                        end
                    end
                    for i=1:size(analysis,1);
                        if analysis{i,1}{1}==list(pair,:);
                            analysis{i,1}{3}{FF}=NoiseCorr;
                        end
                    end
                end
            end
        end
        ANALYSIS{site,1}=analysis;
    end
    filename=d{1,1}{1,1}(1:end-3);
    OUTPUT(date,:)={filename,ANALYSIS};
end
end