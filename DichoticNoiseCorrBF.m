addpath('C:\Users\mbeck\Desktop\Code\other')
%%
load('C:\Users\mbeck\Desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted_2.mat')
%%

count = 1;
pattern = 'b.';
% window = [0.1,0.15,0.2,0.3];
window = 0.15;

NC = cell(length(window),3);
% SENSE = cell(length(window),3);

for R = 1:3
    switch R
        case 1
            data = OT;
            disp('Loading OT')
        case 2
            data = FL;
            disp('Loading FL')
        case 3
            data = AAr;
            disp('Loading AAr')
    end
    
    for w = 1:length(window)
        disp(['Window - ' num2str(window(w))])
        
        NC{w,R} = nan(5000,2);
%         SENSE{w,R} = nan(500,1);
        
        for date = 1:length(data)
            for site = 1:length(data{date})
                list = combnk(2:length(data{date}{site}),2);
                for pair = 1:size(list,1)
                    unit1 = fieldnames(data{date}{site}{list(pair,1)});
                    unit2 = fieldnames(data{date}{site}{list(pair,2)});
                    unit1 = unit1(~cellfun(@isempty,regexpi(unit1,pattern)));
                    unit2 = unit2(~cellfun(@isempty,regexpi(unit2,pattern)));
                    filenames = intersect(unit1,unit2);
                    clear unit1 unit2
                    
                    if ~isempty(filenames)
                        
                        holder = nan(1000,2);
%                         counter = 0;
                        
                        for files = 1:length(filenames)
                            
                            U1 = data{date}{site}{list(pair,1)}.(filenames{files});
                            U2 = data{date}{site}{list(pair,2)}.(filenames{files});

%                             sens1 = find(mean(U1.spikes_count,2) >= mean(U1.base_count,2)+2*std(U1.base_count,0,2));
%                             sens2 = find(mean(U2.spikes_count,2) >= mean(U2.base_count,2)+2*std(U2.base_count,0,2));
%                             sens1 = find(mean(U1.spikes_count,2) >= mean(U1.base_count(:))+2*std(U1.base_count(:)));
%                             sens2 = find(mean(U2.spikes_count,2) >= mean(U2.base_count(:))+2*std(U2.base_count(:)));
                            sens1 = find(U1.sensitive == 1);
                            sens2 = find(U2.sensitive == 1);
                            sens = intersect(sens1,sens2);
%                             counter = counter+length(sens);
                            clear sens1 sens2
                            
                            GM = sqrt(mean(U1.spikes_count,2).*mean(U2.spikes_count,2));
%                             GM = norm01(GM,2);
                            GM = GM(sens);
%                             best = find(GM==max(GM),1,'first');

%                             sens = intersect(sens,best);
%                             sens = 1:length(GM);
%                             sens = sens(best);
                            
                            if ~isempty(sens)

                                for s = 1:length(sens)
                                    
                                    unit1 = U1.spikes_times(sens(s),:);
                                    unit2 = U2.spikes_times(sens(s),:);
                                    
                                    check = cellfun(@(x) x<window(w), unit1, 'UniformOutput', false);
                                    for i = 1:length(check)
                                        unit1{i}=unit1{i}(check{i});
                                    end
                                    unit1 = cellfun(@length,unit1);
                                    
                                    check = cellfun(@(x) x<window(w), unit2, 'UniformOutput', false);
                                    for i = 1:length(check)
                                        unit2{i}=unit2{i}(check{i});
                                    end
                                    unit2 = cellfun(@length,unit2);
                                    
                                    if length(unique(unit1))>2 && length(unique(unit2))>2 || sum(max(unit1) == unit1) > 1 && max(unit1) > 0 || sum(max(unit2) == unit2) > 1 && max(unit2) > 0
                                        c = corr(unit1',unit2');
                                        L = find(isnan(holder(:,1)),1,'first');
                                        holder(L,1) = 0.5*log((1+c)/(1-c));
%                                         holder(L,2) = GM(sens(s));
                                        holder(L,2) = GM(s);
                                    end
                                end
                                
                            end
                        end     % FILES 
                         
                        L = find(isnan(holder(:,1)),1,'first');
                        holder(L:end,:)=[];
                        L = find(isnan(NC{w,R}(:,1)),1,'first');
                        NC{w,R}(L,:)=nanmean(holder,1);
%                         SENSE{w,R}(L)=counter/length(filenames);

%                         NC{w,R}(L:L+size(holder,1)-1,:)=holder;
                        
                    end
                    
                end         % PAIR
            end             % SITE
        end                 % DATE
        
        L = find(isnan(NC{w,R}(:,1)),1,'first');
        NC{w,R}(L:end,:)=[];
%         SENSE{w,R}(L:end)=[];
%         figure(count)
%         scatter(NC{w,R}(:,2),NC{w,R}(:,1))
%         axis square
%         ylim([-1 1])
%         switch R
%             case 1
%                 title('OT')
%             case 2
%                 title('FL')
%             case 3
%                 title('AAr')
%         end
%         count = count+1;
    end                     % W
    
end                     % R

clearvars -except OT FL AAr NC SENSE

