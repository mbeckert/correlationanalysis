% AAr
% FILES = {'data_006_2015_0502','data_006_2015_0521','data_006_2015_0616','data_006_2015_0713','data_006_2015_0722', ...
%     'data_021_2015_0528','data_021_2015_0609','data_021_2015_0706','data_021_2015_0721','data_021_2015_0730', ...
%     'data_023_2015_0422','data_023_2015_0501','data_023_2015_0610', ...
%     'data_029_2015_0910','data_029_2015_0929', ...
%     'data_032_2015_0903'};

% OT
% FILES = {'data_029_2015_1029','data_029_2015_1112','data_029_2015_1123','data_029_2015_1214', ...
%     'data_032_2015_1110','data_032_2015_1119','data_032_2016_0223'};

% Field L
FILES = {'data_029_2016_0113','data_029_2016_0510', ...
    'data_032_2016_0106','data_032_2016_0119','data_032_2016_0202','data_032_2016_0503','data_032_2016_0517'};

for date = 1:length(FILES);

    disp(['Loading File ' FILES{date}]);
    load([FILES{date} '.mat']);
    eval(['d=' FILES{date} ';']);
    eval(['clear ' FILES{date}]);

    for SITE=1:size(d,1);
        D=d{SITE}(2:end);
        x=zeros(1,length(D));
        for y=1:length(D);
            if isfield(D{y},'FFf1');
                x(y)=1;
            end
        end
        y=find(x==1);

        if length(y)>1;
            LIST=combnk(y,2);

            for list=1:size(LIST,1);
                sens=intersect(find(D{LIST(list,1)}.FFf1.sensitive==1),find(D{LIST(list,2)}.FFf1.sensitive==1));
                z1=reshape(D{LIST(list,1)}.FFf1.spikes_count(sens,:),[1,length(sens)*size(D{LIST(list,1)}.FFf1.spikes_count,2)]);
                z2=reshape(D{LIST(list,2)}.FFf1.spikes_count(sens,:),[1,length(sens)*size(D{LIST(list,2)}.FFf1.spikes_count,2)]);
%                 scatter(z1,z2,'jitter','on','jitterAmount',0.2);
                CORR=corrcoef(z1,z2);
                if CORR>0.1;
                z1=zscore(z1);
                z2=zscore(z2);
                cloudplot(z1,z2,5);
                title(['data - ' num2str(date) '  site - ' num2str(SITE) '  unit - ' num2str(LIST(list,1)) '  unit - ' num2str(LIST(list,2)) '  R - ' num2str(CORR(1,2))]);
                pause
                end
            end
        end
    end

end
