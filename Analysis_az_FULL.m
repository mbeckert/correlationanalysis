%% Load raw data
disp('Loading Raw Data')
load('C:\Users\mbeck\Desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted');
disp('Done')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% Gather Constants from Speaker Index
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Setting constants')
FFflip=[3,2,1,8,7,6,5,4,15,14,13,12,11,10,9,24,23,22,21,20,19,18,17,16,31,30,29,28,27,26,25,40,39,38,37,36,35,34,33,32,51,50,49,48,47,46,45,44,43,42,41,62,61,60,59,58,57,56,55,54,53,52,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,64,63,94,93,92,91,90,89,88,87,86,85,84,105,104,103,102,101,100,99,98,97,96,95,114,113,112,111,110,109,108,107,106,121,120,119,118,117,116,115,130,129,128,127,126,125,124,123,122,137,136,135,134,133,132,131,142,141,140,139,138,144,143];
load('C:\Users\mbeck\Desktop\Data\Analysis\SpeakerIndex','SpeakerIndex');
[B,I]=sortrows(SpeakerIndex(:,1));
H=hist(B,-100:10:100);
H=cumsum(H);
clear B SpeakerIndex X Y x
disp('Done')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% Convert FF data into Azimuth tuning curves and correct for hemispheres
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Converting FF data')
for r = 1:3
    out=[];
    index=[];
    if r==1; data=OT; Right = [3,4,6]; end
    if r==2; data=FL; Right = 2; end
    if r==3; data=AAr; Right = [6,7,8,10,16]; end

    for date=1:length(data)
        for site = 1:length(data{date})
            for unit = 2:length(data{date}{site})
                d=data{date}{site}{unit};
                filenames=fieldnames(d);
                holder=[];
                for file=1:length(filenames)
                    if sum(filenames{file}(1:2)=='FF')==2
                        holder=[holder,d.(filenames{file}).spikes_count];
                    end
                end
                if ~isempty(holder)
                    if sum(date==Right)
                        holder=holder(FFflip,:);
                    end
                    holder=holder(I,:);
                    idx=size(out,1)+1;
                    out(idx,:)=nan(1,length(H));
                    
                    % use this to average across all trials of each speaker
                    
%                     temp=holder(1:H(1),:);
%                     out(idx,1)=nanmean(temp(:));
%                     for point=2:length(H);
%                         temp=holder(H(point-1):H(point),:);
%                         out(idx,point)=nanmean(temp(:));
%                     end
%                     index(idx,:)=[date,site,unit];

                    % use this to average across the average of each
                    % speaker
                    
                    temp=nanmean(holder,2);
                    out(idx,1)=nanmean(temp(1:H(1)));
                    for point=2:length(H);
                        out(idx,point)=nanmean(temp(H(point-1):H(point)));
                    end
                    index(idx,:)=[date,site,unit];
                    

                    % These two methods are equivalent as long as the
                    % speakers have the same number of trials, which for
                    % most data they should.

                end                
            end % unit
            
            clear d filenames file temp holder idx point 
            
        end % site
    end % date

if r == 1; curve_ot = out; idx_ot = index; end
if r == 2; curve_fl = out; idx_fl = index; end
if r == 3; curve_aar = out; idx_aar = index;end
    
clear out index Right date data site unit d filenames

end % r

clear r H FFflip I
       
disp('Done')
%%
addpath('C:\Users\mbeck\Desktop\Code\other')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% smooth and normalize curves
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Smooth and Normalizing Curves')
for r = 2:3
    if r == 1; d=curve_ot; end
    if r == 2; d=curve_fl; end
    if r == 3; d=curve_aar; end

    x=nan(size(d));

    for i = 1:size(x,1)
        x(i,:)=smooth(d(i,:),3)';
        x(i,:)=norm01(x(i,:));
    end

    if r == 1; sm_O=x; end
    if r == 2; sm_F=x; end
    if r == 3; sm_A=x; end

end

clear r x i d
disp('Done')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% Run the signal correlation for distant neurons
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Running Signal Correlation for Distant Neurons')
% change this value to run analyis for subdivisions
% 1:6=ipsi, 7-14=front, 15:21=contra;
bounds=[1,21; 1,7; 8,14; 15,21];

for b = 1:4
    for r = 1:3

        if r == 1; d=curve_ot(:,bounds(b,1):bounds(b,2)); i = idx_ot; end
        if r == 2; d=sm_F(:,bounds(b,1):bounds(b,2)); i = idx_fl; end
        if r == 3; d=sm_A(:,bounds(b,1):bounds(b,2)); i = idx_aar; end
        
        out=[];

        list=combnk(1:size(d,1),2);
        for pair = 1:size(list,1) 
                    if i(list(pair,1),1)~=i(list(pair,2),1) && i(list(pair,1),2)~=i(list(pair,2),2)
                        c=corr(d(list(pair,1),:)',d(list(pair,2),:)');
                        out(length(out)+1,1)=0.5*log((1+c)/(1-c));
                        out(length(out)+1,1) = c;
                    end
        end

        if r == 1; SIG_d_O{b}=out; end
        if r == 2; SIG_d_F{b}=out; end
        if r == 3; SIG_d_A{b}=out; end

        clear d list out c pair list i

    end
end

clear r bounds b
disp('Done')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% Run signal correlation for nearby neurons
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Running Signal Correlation for Nearby Neurons')

for r = 1:3

    if r == 1; d=curve_ot; i = idx_ot; end
    if r == 2; d=sm_F; i = idx_fl; end
    if r == 3; d=sm_A; i = idx_aar; end

    out = [];

    for date=1:max(i(:,1))
        for site=1:max(i(:,2))
            x1=find(i(:,1)==date);
            x2=find(i(:,2)==site);
            idx=intersect(x1,x2);
            if ~isempty(idx)
                list=combnk(idx,2);
                for pair=1:size(list,1)
                    c=corr(d(list(pair,1),:)',d(list(pair,2),:)');
                    out(length(out)+1)=0.5*log((1+c)/(1-c));
                    out(length(out)+1) = c;
                end
            end
        end
    end

    if r == 1; SIG_n_O=out; end
    if r == 2; SIG_n_F=out; end
    if r == 3; SIG_n_A=out; end

    clear d list out i x1 x2 idx x pair date site c

end

clear r
disp('Done')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% compile the signal correlation results into a matrix for plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
disp('Compiling SigCorr data into matrix')

for r = 2:3
    if r == 1; d=SIG_d_O; n=SIG_n_O; end
    if r == 2; d=SIG_d_F; n=SIG_n_F; end
    if r == 3; d=SIG_d_A; n=SIG_n_A; end
    
    out{r}=[];
    
    m=cellfun(@length,d);
    m=[m,length(n)];
    m=max(m);
    
    out{r}(:,1)=[n';nan(m-length(n),1)];
    for i=1:4
        out{r}(:,i+1)=[d{i};nan(m-length(d{i}),1)];
    end
    
    clear m i d n
    
end

clear r

d_a = SIG_d_A; d_f = SIG_d_F;
n_a = SIG_n_A; n_f = SIG_n_F; n_o = SIG_n_O;

m(1)=length(d_a{1});
m(2)=length(d_f{1});
m(3)=length(n_a);
m(4)=length(n_f);
m(5)=length(n_o);

d_m=max(m(1:2));
n_m=max(m(3:5));

nearby(:,1)=[n_o';nan(n_m-length(n_o),1)];
nearby(:,2)=[n_f';nan(n_m-length(n_f),1)];
nearby(:,3)=[n_a';nan(n_m-length(n_a),1)];
distant(:,1)=[d_f{1};nan(d_m-length(d_f{1}),1)];
distant(:,2)=[d_a{1};nan(d_m-length(d_a{1}),1)];

clear m d_* n_*

disp('Done')

disp('Complete!')

