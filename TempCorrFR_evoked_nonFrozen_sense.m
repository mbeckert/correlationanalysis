%% First step is to calculate reproducibility from the raw data

load('C:\Users\mbeck\Desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted.mat');

%%

%%

% % output organization
% % "Synch"
% % Synch{region}.filetypes{pairs}[date site unit1 unit2 PEAK unit1FR unit2FR (check if sig CCG)]
% %                                ... for each depvar
% % bandwidth, AUC, AUC of main peak

% Create the data structure

% bin = [0.001,0.002,0.005];
bin = 0.001;
Synch=cell(3,length(bin));

Window = sort([-0.1 0.1]);
dur = 0.15;
pattern = 'FFf.|frozen.|b.';

for b = 1:length(bin)
    binWidth = bin(b);
    binNum = length(Window(1):binWidth:Window(2))-2;
    for R=1:3
        switch R
            case 1
                data = OT;
            case 2
                data = FL;
            case 3
                data = AAr;
        end
        
        D=[];
        
        for date=1:size(data,2)
            for site = 1:length(data{date})
                
                list=combnk(2:length(data{date}{site}),2);
                
                for pair = 1:size(list,1)
                    
                    disp(['region: ' num2str(R) '; date: ' num2str(date) '; site: ' num2str(site) '; pair: ' num2str(pair)]);
                    
                    unit1 = data{date}{site}{list(pair,1)};
                    unit2 = data{date}{site}{list(pair,2)};
                    
                    filetypes1 = fieldnames(unit1);
                    filetypes2 = fieldnames(unit2);
                    
                    filetypes = intersect(filetypes1,filetypes2);
                    
                    idx = regexpi(filetypes,pattern);
                    idx = cellfun(@isempty,idx);
                    filetypes = filetypes(idx);
                    
                    clear filetypes1 filetypes2 idx
                    
                    for file = 1:length(filetypes)
                        
                        CCG = zeros(length(unit1.(filetypes{file}).depvar),binNum);
                        shiftCCG = zeros(length(unit1.(filetypes{file}).depvar),binNum);
                        
                        for depvar = 1:length(unit1.(filetypes{file}).depvar)
                                for trial = 1:unit1.(filetypes{file}).reps
                                    for spike = 1:length(unit1.(filetypes{file}).base_times{depvar,trial})
                                        
                                        times = unit1.([filetypes{file}]).base_times{depvar,trial}(spike) - unit2.([filetypes{file}]).base_times{depvar,trial};
                                        his=hist(times,Window(1):binWidth:Window(2)); %% Here is where you can change binwidth
                                        CCG(depvar,:)=CCG(depvar,:)+his(2:end-1);
                                        if trial==unit1.([filetypes{file}]).reps
                                            times = unit1.([filetypes{file}]).base_times{depvar,trial}(spike) - unit2.([filetypes{file}]).base_times{depvar,1};
                                        else
                                            times = unit1.([filetypes{file}]).base_times{depvar,trial}(spike) - unit2.([filetypes{file}]).base_times{depvar,trial+1};
                                        end
                                        his=hist(times,Window(1):binWidth:Window(2));
                                        shiftCCG(depvar,:)=shiftCCG(depvar,:)+his(2:end-1);
                                        
                                    end
                                    
                                end % for "trial"
                        end     % for "depvar" the first time
                        
                        clear his spike times
                        
                        holder = cell(1,9);
                        holder{1} = [date,site,list(pair,1),list(pair,2)];
                        for i = 2:8
                            holder{i} = nan(1,length(unit1.([filetypes{file}]).depvar));
                        end
                        holder{9}=cell(1,length(unit1.([filetypes{file}]).depvar));
                        
                        for depvar = 1:length(unit1.([filetypes{file}]).depvar)
                            
                            rate1=mean(unit1.([filetypes{file}]).base_count(depvar,:))/dur;
                            rate2=mean(unit2.([filetypes{file}]).base_count(depvar,:))/dur;
                            
                            SmCCG=(smooth(CCG(depvar,:))/sqrt(rate1*rate2))/binNum;
                            SmShiftCCG=(smooth(shiftCCG(depvar,:))/sqrt(rate1*rate2))/binNum;
                            
                            SmCCGCorrected=SmCCG-SmShiftCCG;
                            
                            flankS=std([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                            flankM=mean([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                            
                            PEAK=SmCCGCorrected(round(binNum/2));
                            check = PEAK>flankM+5*flankS;
                            
                            positive=(SmCCGCorrected>=check);
                            pos=diff(positive);
                            less=pos(1:round(binNum/2));
                            more=pos(round(binNum/2):end);
                            Low=find(less==1,1,'last');
                            High=find(more==-1,1,'first');
                            Low=length(less)-Low;
                            bandPEAK=High+Low;
                            
                            if isempty(bandPEAK)
                                bandPEAK=nan;
                                sumband=nan;
                            else
                                sumband=sum(SmCCGCorrected(length(less)-Low:length(less)+High))/bandPEAK;
                            end
                            
                            holder{2}(1,depvar)=PEAK;
                            holder{3}(1,depvar)=rate1;
                            holder{4}(1,depvar)=rate2;
                            holder{5}(1,depvar)=check;
                            holder{6}(1,depvar)=bandPEAK;
                            holder{7}(1,depvar)=sum(SmCCGCorrected);
                            holder{8}(1,depvar)=sumband;
                            holder{9}{1,depvar}=SmCCG;
                            holder{10}{1,depvar}=SmShiftCCG;
                            holder{11}{1,depvar}=SmCCGCorrected;
                            
                            clear positive pos less more Low High bandPEAK
                            
                        end     % for "depvar" the second time
                        
                        if size(holder{2},2)<20
                            for i = 2:8
                                holder{i}=padarray(holder{i},[0 21-size(holder{i},2)],'post');
                                holder{i}(holder{i}==0)=nan;
                            end
                            x=cell(1,21-size(holder{9},2));
                            holder{9}=[holder{9}, x];
                            holder{10}=[holder{10}, x];
                            holder{11}=[holder{11}, x];
                            clear x
                        end
                        if isfield(D,([filetypes{file}]))
                            for i = 1:length(holder)
                                D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i}];
                            end
                        else
                            for i = 1:length(holder)
                                D.([filetypes{file}]){i}=holder{i};
                            end
                        end
                        
                        clear holder depvar rate* i PEAK check SmCCG curves CORR trial flank* sumband SmShiftCCG shiftCCG SmCCGCorrected CCG
                        
                    end         % for "file"
                    
                    clear filetypes
                    
                end             % for "pair"
            end                 % for "site"
        end                     % for "date"
        
        Synch{R,b} = D;
        % Synch{1} = D;
        
        clear date file site pair list unit1 unit2 data D
        
    end                         % for "region"
end                         % for "bin"
clearvars -except Synch* AAr OT FL

%% extract the data from the "Synch" structure made above

% "data" structure

load('Synch_02.mat','Synch');

DATA_SYNandFR=cell(1,1);
KEY=cell(1,1);

for region = 1:3
    data = Synch{region};
    filetypes = fieldnames(data);
    
    for file = 1:length(filetypes)
        info = data.([filetypes{file}]){1};
        for pair = 1:size(data.([filetypes{file}]){1,1},1)
            check=data.([filetypes{file}]){5}(pair,:)==0;
            syn=data.([filetypes{file}]){2}(pair,:);
            band=data.([filetypes{file}]){6}(pair,:);
            auc=data.([filetypes{file}]){7}(pair,:);
            aucpeak=data.([filetypes{file}]){8}(pair,:);
            FR1=data.([filetypes{file}]){3}(pair,:)/0.15;
            FR2=data.([filetypes{file}]){4}(pair,:)/0.15;
            FR=nan(size(FR1));
            for i=1:length(FR(:))
                FR(i)=sqrt(FR1(i)*FR2(i));
            end
            %             noise=data.([filetypes{file}]){9}(pair,:);
            curve=data.([filetypes{file}]){9}(:,pair);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Use this to either eliminate non-responsive stimuli or not
            % either making it a nan or zero, or including
            correct = 0;
            
            syn(check)=correct;
            band(check)=correct;
            auc(check)=correct;
            aucpeak(check)=correct;
            %             FR(check)=correct;
            %             FR1(check)=correct;
            %             FR2(check)=correct;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            holder{1} = syn;
            holder{2} = band;
            holder{3} = auc;
            holder{4} = aucpeak;
            holder{6} = FR;
            holder{7} = FR1;
            holder{8} = FR2;
            holder{5} = curve;
            
            if isfield(DATA_SYNandFR{region},([filetypes{file}]))
                for i = 1:length(holder)
                    DATA_SYNandFR{region}.([filetypes{file}]){i}=[DATA_SYNandFR{region}.([filetypes{file}]){i};holder{i}];
                end
            else
                for i = 1:length(holder)
                    DATA_SYNandFR{region}.([filetypes{file}]){i}=holder{i};
                end
            end
            KEY{region}.([filetypes{file}])=info;
            
        end             % for "pair"
    end                 % for "file"
end                     % for "region"

clear check rep FR file pair region data filetypes noise FR1 FR2 i info band syn auc* unit1 unit2 Synch* correct holder

%% Merge the individual filetypes into the broader files

itd={'itd','ITD'};
ild={'ild','ILD'};
ff={'FF1','FF2','FF3','FF4','FF5','FFf1','FFf2','FFf3','FFf4','FFf5'};

ILD=cell(1,1);
ITD=cell(1,1);
FF=cell(1,1);

for curve = 1:3;
    if curve == 1; C=itd; end
    if curve == 2; C=ild; end
    if curve == 3; C=ff; end
    
    D=cell(3,8);
    K=cell(3,1);
    
    for region = 1%:3;
        
        for file = 1:length(C);
            if isfield(DATA_SYNandFR{region},([C{file}]));
                for met = 1:size(D,2);
                    D{region,met}=[D{region,met};DATA_SYNandFR{region}.([C{file}]){met}];
                end
                K{region}=[K{region};KEY{region}.([C{file}])];
            end
        end             % for "file"
    end                 % for "region"
    
    if curve == 1; ITD=D; KEY_ITD=K; end
    if curve == 2; ILD=D; KEY_ILD=K; end
    if curve == 3; FF=D; KEY_FF=K; end
    
end                     % for "curve"

clear curve region file itd ild ff D C DATA_SYNandFR met KEY* K

save SynchFR_temp

%% Use the outside function to convert the FF data into Azimuth and Elevation curves

SpRF2HorVer_Synch

%% Normalize the curves

for type = 1:4;
    
    if type == 1; data = ITD; end
    if type == 2; data = ILD; end
    % if type == 3; data = FF; end
    if type == 3; data = Az; end
    if type == 4; data = El; end
    
    for region = 1:3;
        for met = 1:size(data,2);
            data{region,met}=norm01(data{region,met});
        end
    end
    
    if type == 1; ITD_norm = data; end
    if type == 2; ILD_norm = data; end
    % if type == 3; FF_norm = data; end
    if type == 3; Az_norm = data; end
    if type == 4; El_norm = data; end
    
end

clear region pair x y data type met

%% generate histograms based on firing rates

for type = 1:4;
    if type == 1; d = ITD; e = ITD_norm; end
    if type == 2; d = ILD; e = ILD_norm; end
    if type == 3; d = Az; e = Az_norm; end
    if type == 4; d = El; e = El_norm; end
    
    hisN=cell(3,12);
    hisR=cell(3,12);
    key=cell(3,12);
    
    for region = 1:3
        count = 1;
        for metric = 6:8
            for metric2 = 1:5
                key{region,count}=[metric2,metric];
                hisR{region,count}=nan(size(d{region,metric},1),11);
                hisN{region,count}=nan(size(e{region,metric},1),11);
                for pair = 1:size(d{region,metric},1);
                    
                    [B,I]=histc(e{region,metric}(pair,:),0:0.1:1);
                    
                    for bin = 1:11
                        idx=find(I==bin);
                        hisR{region,count}(pair,bin)=nanmean(d{region,metric2}(pair,idx));
                        hisN{region,count}(pair,bin)=nanmean(e{region,metric2}(pair,idx));
                    end
                    
                end         % for "pair"
                count=count+1;
            end
        end             % for "metric"
    end                 % for "region"
    
    if type == 1; ITD_hist_norm = hisN; ITD_hist = hisR; ITD_hist_key = key; end
    if type == 2; ILD_hist_norm = hisN; ILD_hist = hisR; ILD_hist_key = key; end
    if type == 3; Az_hist_norm = hisN; Az_hist = hisR; Az_hist_key = key; end
    if type == 4; El_hist_norm = hisN; El_hist = hisR; El_hist_key = key; end
    
end

clear region metric* pair B I H d e idx bin his* type count key

%% Shift the curves around so the center is the best response

region = 1;

for type = 1:4;
    if type == 1; d = ITD; e = ITD_norm; end
    if type == 2; d = ILD; e = ILD_norm; end
    if type == 3; d = Az; e = Az_norm; end
    if type == 4; d = El; e = El_norm; end
    
    b1 = e;     % for the reference data
    b2 = e;     % for the data shifting
    
    DATA=cell(4,3,2);
    
    for metric1 = 1:5;
        for metric2 = 1:3;
            
            cat = size(b1{region,metric1},2);
            
            DATA{metric1,metric2,1}=nan(size(b1{region,metric1},1),cat*2-1);
            DATA{metric1,metric2,2}=nan(size(b1{region,metric1},1),cat*2-1);
            
            for pair = 1:size(b1{region,metric1},1);
                
                temp=nan(1,cat*2-1);
                best=find(b1{region,metric2+5}(pair,:)==max(b1{region,metric2+5}(pair,:)));
                if ~isempty(best)
                    temp=nan(1,cat*2-1);
                    temp(cat+1-best:cat*2-best)=b1{region,metric2+5}(pair,:);
                    DATA{metric1,metric2,1}(pair,:)=temp;
                    temp=nan(1,cat*2-1);
                    temp(cat+1-best:cat*2-best)=b2{region,metric1}(pair,:);
                    DATA{metric1,metric2,2}(pair,:)=temp;
                end
                
            end
        end
    end
    
    if type == 1; ITD_center = DATA; end
    if type == 2; ILD_center = DATA; end
    if type == 3; Az_center = DATA; end
    if type == 4; El_center = DATA; end
    
    clear DATA
    
end

clear DATA region type d e metric* pair temp best cat b1 b2

%% PLOTTING FUNCTIONS

%% Use these for plotting the curves in different ways

region = 3;

for type = 1:4;
    if type == 1; d = ITD; e = ITD_norm; end
    if type == 2; d = ILD; e = ILD_norm; end
    if type == 3; d = Az; e = Az_norm; end
    if type == 4; d = El; e = El_norm; end
    
    b1=e;   % set this to "d" to check raw data and "e" for normalized
    b2=e;   % set this to "d" to check raw data and "e" for normalized
    
    figure(type)
    subplot(3,4,1); scatter(b1{region,5}(:),b2{region,1}(:)); title(['Type: ' num2str(type) ' - FR geo vs Syn']);
    subplot(3,4,2); scatter(b1{region,5}(:),b2{region,2}(:)); title(['Type: ' num2str(type) ' - FR geo vs band']);
    subplot(3,4,3); scatter(b1{region,5}(:),b2{region,3}(:)); title(['Type: ' num2str(type) ' - FR geo vs AUC']);
    subplot(3,4,4); scatter(b1{region,5}(:),b2{region,4}(:)); title(['Type: ' num2str(type) ' - FR geo vs aucpeak']);
    subplot(3,4,5); scatter(b1{region,6}(:),b2{region,1}(:)); title(['Type: ' num2str(type) ' - FR 1 vs Syn']);
    subplot(3,4,6); scatter(b1{region,6}(:),b2{region,2}(:)); title(['Type: ' num2str(type) ' - FR 1 vs band']);
    subplot(3,4,7); scatter(b1{region,6}(:),b2{region,3}(:)); title(['Type: ' num2str(type) ' - FR 1 vs AUC']);
    subplot(3,4,8); scatter(b1{region,6}(:),b2{region,4}(:)); title(['Type: ' num2str(type) ' - FR 1 vs aucpeak']);
    subplot(3,4,9); scatter(b1{region,7}(:),b2{region,1}(:)); title(['Type: ' num2str(type) ' - FR 2 vs Syn']);
    subplot(3,4,10); scatter(b1{region,7}(:),b2{region,2}(:)); title(['Type: ' num2str(type) ' - FR 2 vs band']);
    subplot(3,4,11); scatter(b1{region,7}(:),b2{region,3}(:)); title(['Type: ' num2str(type) ' - FR 2 vs AUC']);
    subplot(3,4,12); scatter(b1{region,7}(:),b2{region,4}(:)); title(['Type: ' num2str(type) ' - FR 2 vs aucpeak']);
    %     scatter(b1{region,6}(:),b2{region,7}(:)); title(['Type: ' num2str(type) ' - FR unit1 vs FR unit2']);
end

clear d e type region b1 b2

%% plotting histogram data

for region = 1; %:3;
    
    for type = 1:4;
        if type == 1; d = ITD_hist_norm; k = ITD_hist_key; end
        if type == 2; d = ILD_hist_norm; k = ILD_hist_key; end
        if type == 3; d = Az_hist_norm; k = Az_hist_key; end
        if type == 4; d = El_hist_norm; k = El_hist_key; end
        
        figure(type)
        
        for metric = 1:15;
            subplot(3,5,metric);plot(nanmean(d{region,metric},1));
            title(['reference - ' num2str(k{region,metric}(2)) ' : plotted - ' num2str(k{region,metric}(1))]);
        end
        
    end
    
end
clear d type metric region k

%% plotting the center plots data

for region = 1; %:3;
    
    for type = 1:4;
        if type == 1; d = ITD_center; end
        if type == 2; d = ILD_center; end
        if type == 3; d = Az_center; end
        if type == 4; d = El_center; end
        
        figure(type)
        count=0;
        
        for metric1 = 1:4;
            for metric2 = 1:3;
                count = count+1;
                subplot(4,3,count);plot(nanmean(d{metric1,metric2,2},1));
            end
        end
        
    end
    
end
clear d type metric* region count

%%
