%%
load('C:\Users\PenaLab\Google Drive\Correlations Paper\CorrelationsPaper\correlationwork1.mat','CURVE*','KEY*')
%%
% sample = floor(linspace(1,21,7));
sample = floor(linspace(1,21,7));
s1=linspace(1,19,7);
s2=linspace(2,20,7);
s3=linspace(3,21,7);
curvetype = {'Az','itd'};

for c = 1:2
    data=cell(3,1);
    data{1}=[]; data{2}=[]; data{3}=[];
    data2=cell(3,1);
    data2{1}=[]; data2{2}=[]; data2{3}=[];
    
    for r = 1:3
        
        if r == 1; d = CURVE_OT.(curvetype{c}); k= KEY_OT.(curvetype{c}); end
        if r == 2; d = CURVE_FL.(curvetype{c}); k= KEY_FL.(curvetype{c});end
        if r == 3; d = CURVE_AAr.(curvetype{c}); k= KEY_AAr.(curvetype{c});end
        
        for date = 1:max(k(:,1))
            for site = 1:max(k(:,2))
                x1=find(k(:,1)==date);
                x2=find(k(:,2)==site);
                neu=intersect(x1,x2);
                if ~isempty(neu)
                    list = combnk(neu,2);
                    for pair=1:size(list,1)
                        x1=smooth(d(list(pair,1),:));
                        x2=smooth(d(list(pair,2),:));
%                         x1=d(list(pair,1),:)';
%                         x2=d(list(pair,2),:)';
                        co1=corr(x1,x2);
%                         X1=nan(7,1);
%                         X2=X1;
%                         for g=1:7;
%                             X1(g)=nanmean(x1([s1(g),s2(g),s3(g)]));
%                             X2(g)=nanmean(x2([s1(g),s2(g),s3(g)]));
%                         end
%                         x1=X1;x2=X2;
                        co2=corr(x1(sample),x2(sample));
                        co1=0.5*log((1+co1)/(1-co1));
                        co2=0.5*log((1+co2)/(1-co2));
                        data{r}=[data{r};co1];
                        data2{r}=[data2{r};co2];
                    end % pair
                end % neu check
                
            end % site
        end % date
    end % r
    
    clear r site x1 x2 neu list pair d k date co1 co2 X* g
    
    % sigcorr structure
    % Az or itd
    % first cell is FULL, second cell is SAMPLE
    sigcorr.(curvetype{c}){2}=data2;
    sigcorr.(curvetype{c}){1}=data;
    
    clear data*
    
end % c

clear c curvetype sample s1 s2 s3

%%

curvetype = {'Az','itd'};
count=1;

for c = 1:2
    for r = 1:3
        figure(count)
%         x1=sigcorr.(curvetype{c}){1}{r}(sigcorr.(curvetype{c}){1}{r}>=0);
%         x2=sigcorr.(curvetype{c}){2}{r}(sigcorr.(curvetype{c}){1}{r}>=0);
        x1=sigcorr.(curvetype{c}){1}{r};
        x2=sigcorr.(curvetype{c}){2}{r};
        scatter(x1,x2)
        [H,P]=ttest(x1,x2);
        [rho,p]=corr(x1,x2,'rows','pairwise');
%         title([curvetype{c} '; region = ' num2str(r) '; p = ' num2str(P) '; rho = ' num2str(rho)])
        title(['rho = ' num2str(rho) ' ; p = ' num2str(p)])
        axis([-1 3.5 -1 3.5]); 
        xlabel('Full')
        ylabel('Sample')  
        count=count+1;
    end % r
end % c

clear curvetype count c r P x* H rho p

%%

curvetype = {'Az','itd'};
d = cell(2,3);

for c = 1:2
    for r = 1:3
        d{c,r}=sigcorr.(curvetype{c}){1}{r}./sigcorr.(curvetype{c}){2}{r};
%         d{c,r}=sigcorr.(curvetype{c}){1}{r}-sigcorr.(curvetype{c}){2}{r};
        d{c,r}(d{c,r}>quantile(d{c,r},0.99))=nan;
        d{c,r}(d{c,r}<quantile(d{c,r},0.01))=nan;
    end % r
end % c

x=cellfun(@length,d);
x=max(max(x));

data=nan(x,6);
count=1;
for c = 1:2
    for r = 1:3
        fill=nan(x-length(d{c,r}),1);
        data(:,count)=[d{c,r};fill];
        count=count+1;
    end
end

clear c r curvetype count x d fill
%%

data=nan(2,3);

for c = 1:2
    for r = 1:3
        data(c,r)=quantile(d{c,r},0.5);
    end
end
