% Use "TempCorrFR" to generate a Synchrony data file. Load that here to
% extract the desired parameters for stats and plotting.

%%
load('C:\Users\mbeck\Desktop\Data\Analysis\CorrelationsPaper\SynchRAWdata.mat');
%%
for b = 1:3
for s = 1:2
    switch s
        case 1
           d = SynchBase(:,b);
        case 2
            d = SynchEvoked(:,b);
    end

out = cell(1,3);
KEY = cell(1,3);


for R = 1:3
    out{R} = nan(500,4);
    KEY{R} = nan(500,4);
    filenames = fieldnames(d{R});
    for date = 1:10
        for site = 1:10
            for unit1 = 1:10
                for unit2 = 1:10
                    check = [];
                    band = [];
                    sumband = [];
                    auc = [];
                    for file = 1:length(filenames)
                        key = d{R}.(filenames{file}){1};
                        i1 = find(key(:,1) == date);
                        i2 = find(key(:,2) == site);
                        i3 = find(key(:,3) == unit1);
                        i4 = find(key(:,4) == unit2);
                        idx = intersect(i1,(intersect(i2,(intersect(i3,i4)))));
                        check = [check, d{R}.(filenames{file}){5}(idx,:)];
                        band = [band, d{R}.(filenames{file}){6}(idx,:)];
                        sumband = [sumband, d{R}.(filenames{file}){8}(idx,:)];
                        auc = [auc, d{R}.(filenames{file}){7}(idx,:)];
                    end         % FILE
                    if ~isempty(band)
                        L = find(isnan(out{R}(:,1)),1,'first');
                        out{R}(L,1) = sum(check)/length(check);
                        out{R}(L,2) = nanmean(band(:)); % change this to what metric you want to go with
                        out{R}(L,3) = nanmean(sumband(:));
                        out{R}(L,4) = nanmean(auc(:));
                        KEY{R}(L,:) = [date,site,unit1,unit2];
                        disp(['R = ' num2str(R) '; D = ' num2str(date) '; S = ' num2str(site) '; U1 = ' num2str(unit1) '; U2 = ' num2str(unit2)])
                    end         % EMPTY
                end             % UNIT2
            end                 % UNIT1
        end                     % SITE
    end                         % DATE
    
    out{R}(L:end,:)=[];
    KEY{R}(L:end,:)=[];
    
end                             % R

switch s
    case 1
        B = [KEY;out];
    case 2
        E = [KEY;out];
end

end

switch b
    case 1
        S01 = [B,E];
    case 2
        S02 = [B,E];
    case 3
        S05 = [B,E];
    case 4
        S10 = [B,E];
    case 5
        S20 = [B,E];
end
end

clearvars -except S0* S10 S20 Synch*


%%

d = S01;
d = d(2,:);    
    
m = max(cellfun(@length,d));
for i = 1:length(d)
    d{i}=[d{i};nan(m-length(d{i}),4)];
end

out = cell2mat(d);

for j = 1:4
 
switch j
    case 1
        x = 1:4:21;
        check = out(:,x);
    case 2
        x = 2:4:22;
        band = out(:,x);
    case 3
        x = 3:4:23;
        sumband = out(:,x);
    case 4
        x = 4:4:24;
        auc = out(:,x);
end

end

clear data d m i j out x