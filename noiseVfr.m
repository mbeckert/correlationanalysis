cd('C:\Users\mbeck\Desktop\Data\Analysis\CorrelationsPaper\');
load('FullCorrelationAnalysis_3.mat','FullOT','FullAAr','FullFL');
%%

i = [5,15];

data{1} = FullOT(:,i);
data{2} = FullFL(:,i);
data{3} = FullAAr(:,i);

clear Full* i

%%

output = cell(3,1);
bins = 0:0.5:4.5;

for r = 1:3
    output{r}=cell(1,length(bins));
for i = 1:length(bins)-1
    output{r}{i}=data{r}(data{r}(:,1)>bins(i) & data{r}(:,1)<bins(i+1),2);
end
end

%%

for r = 1:3
    l = cellfun(@length,output{r});
    l = max(l);
    for i = 1:length(output{r})
        output{r}{i}=[output{r}{i};nan(l-length(output{r}{i}),1)];
    end
    output{r} = cell2mat(output{r});
end

