function [data]=NoiseCorrelationDichotic

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
% AAr
% FILES = {'data_006_2015_0502','data_006_2015_0521','data_006_2015_0616','data_006_2015_0713','data_006_2015_0722', ...
%     'data_021_2015_0528','data_021_2015_0609','data_021_2015_0706','data_021_2015_0721','data_021_2015_0730', ...
%     'data_023_2015_0422','data_023_2015_0501','data_023_2015_0610', ...
%     'data_029_2015_0910','data_029_2015_0929', ...
%     'data_032_2015_0903'};

% OT
% FILES = {'data_029_2015_1029','data_029_2015_1112','data_029_2015_1123','data_029_2015_1214', ...
%     'data_032_2015_1110','data_032_2015_1119','data_032_2016_0223'};

% Field L
FILES = {'data_029_2016_0113','data_029_2016_0510', ...
    'data_032_2016_0106','data_032_2016_0119','data_032_2016_0202','data_032_2016_0503','data_032_2016_0517'};

data=cell(length(FILES),1);

for date = 1:length(FILES);

    disp(['Loading File ' FILES{date}]);
    load([FILES{date} '.mat']);
    eval(['d=' FILES{date} ';']);
    eval(['clear ' FILES{date}]);
    
    for site=1:length(d);
        for trial=1:5;
            units=2:size(d{site},1);
            checker=zeros(1,length(units));
            for h=2:size(d{site},1);
                if isfield(d{site,1}{h,1},['Frozen' num2str(trial)]) || isfield(d{site,1}{h,1},['frozen' num2str(trial)])
                    checker(h-1)=1;
                end
            end
            checker=logical(checker);
            units=units(checker);
            clear h checker
            if length(units)>1;
            list=combnk(units,2);
            for pair=1:size(list,1);
                NoiseCorr{pair}(trial,1:6)=0;
                CORR=corrcoef(d{site,1}{list(pair,1),1}.(['Frozen' num2str(trial)]).spikes_count,d{site,1}{list(pair,2),1}.(['Frozen' num2str(trial)]).spikes_count,'rows','complete');
                if isnan(CORR(1,2));
                    check = 0;
                else
                    if length(unique(d{site,1}{list(pair,1),1}.(['Frozen' num2str(trial)]).spikes_count))>2 && length(unique(d{site,1}{list(pair,2),1}.(['Frozen' num2str(trial)]).spikes_count))>2;
                        check = 1;
                    elseif sum(d{site,1}{list(pair,1),1}.(['Frozen' num2str(trial)]).spikes_count)==max(d{site,1}{list(pair,1),1}.(['Frozen' num2str(trial)]).spikes_count)>1 || sum(d{site,1}{list(pair,2),1}.(['Frozen' num2str(trial)]).spikes_count)==max(d{site,1}{list(pair,2),1}.(['Frozen' num2str(trial)]).spikes_count)>1;
                        check = 1;
                    else
                        check = 0;
                    end
                end
                NoiseCorr{pair}(trial,1)=list(pair,1);NoiseCorr{pair}(trial,2)=list(pair,2);
                NoiseCorr{pair}(trial,4)=check;
                NoiseCorr{pair}(trial,3)=CORR(1,2);
                NoiseCorr{pair}(trial,5)=nanmean(corrcoef(d{site,1}{list(pair,1),1}.(['Frozen' num2str(trial)]).spikes_count));
                NoiseCorr{pair}(trial,6)=nanmean(corrcoef(d{site,1}{list(pair,2),1}.(['Frozen' num2str(trial)]).spikes_count));
            end
            end
        end
        if exist('NoiseCorr','var');
            DATA{site}=NoiseCorr;
            clear NoiseCorr
        end
    end
    if exist('DATA','var');
        data{date}=DATA;
        clear DATA;
    end
end
end
