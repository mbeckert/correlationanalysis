load('FullCorrelationAnalysis_3.mat');
load('SigCorr_regions.mat');

N=noise_FL;
Tb=temp_FL_b;
T=temp_FL_e;
S=Corr_ipsi_FL_in;
FILES=7; % AAr = 16; OT & Field L = 7

list=combnk(1:20,2);

data=[];

for date=1:FILES;
    for site=1:10;
        for pair=1:size(list,1);
            GM=[];
            noise=[];
            z=nan;
            for DATE=1:size(N,1);
                for SITE=1:size(N{DATE,2},1);
                    for PAIR=1:size(N{DATE,2}{SITE,1},1);
                        if N{DATE,2}{SITE,1}{PAIR,1}{1,1}(1)==date && N{DATE,2}{SITE,1}{PAIR,1}{1,1}(2)==site && N{DATE,2}{SITE,1}{PAIR,1}{1,1}(3)==list(pair,1) && N{DATE,2}{SITE,1}{PAIR,1}{1,1}(4)==list(pair,2);
                            noise=N{DATE,2}{SITE,1}{PAIR,1}{3};
                        elseif N{DATE,2}{SITE,1}{PAIR,1}{1,1}(1)==date && N{DATE,2}{SITE,1}{PAIR,1}{1,1}(2)==site && N{DATE,2}{SITE,1}{PAIR,1}{1,1}(3)==list(pair,2) && N{DATE,2}{SITE,1}{PAIR,1}{1,1}(4)==list(pair,1);
                            noise=N{DATE,2}{SITE,1}{PAIR,1}{3};
                        end
                    end
                end
            end
            if ~isempty(noise);
                if ~isempty(noise{1,end});
                    z=[];
                    z1=[];
                    z2=nan(1,2);
                    for F=1:size(noise,2);
                        if ~isempty(noise{1,F});
%                             x=isnan(noise{1,F}{1,end}(:,1));
%                             noise{1,F}{1,end}(x,:)=[];
%                             x=noise{1,F}{1,end}(:,2)==0;
%                             noise{1,F}{1,end}(x,:)=[];
%                             z1=noise{1,F}{1,end}(:,1);
                            
                            x=isnan(noise{1,F}(:,1));
                            noise{1,F}(x,:)=[];
                            x=noise{1,F}(:,2)==0;
                            noise{1,F}(x,:)=[];
                            z1=noise{1,F}(:,1);
                            
                            for c=1:length(z1);
                                z1(c)=0.5*log((1+z1(c))/(1-z1(c)));
                            end
                            z2(F)=nanmean(z1,1);
                        end
                    end
                    z=nanmean(z2);
                else
                    noise=nan;
                    z=nan;
                end
            else
                noise=nan;
            end
            time=[nan nan];
            for LINE=1:size(T,1);
                if T(LINE,1)==date && T(LINE,2)==site && T(LINE,3)==list(pair,1) && T(LINE,4)==list(pair,2);
                    time=T(LINE,9:10);
                elseif T(LINE,1)==date && T(LINE,2)==site && T(LINE,3)==list(pair,2) && T(LINE,4)==list(pair,1);
                    time=T(LINE,9:10);
                end
            end
            timeb=[nan nan];
            for LINE=1:size(Tb,1);
                if Tb(LINE,1)==date && Tb(LINE,2)==site && Tb(LINE,3)==list(pair,1) && Tb(LINE,4)==list(pair,2);
                    timeb=Tb(LINE,9:10);
                elseif Tb(LINE,1)==date && Tb(LINE,2)==site && Tb(LINE,3)==list(pair,2) && Tb(LINE,4)==list(pair,1);
                    timeb=Tb(LINE,9:10);
                end
            end
            ild=nan;
            for LINE=1:size(S.ild,1);
                if S.ild(LINE,1)==date && S.ild(LINE,2)==site && S.ild(LINE,3)-1==list(pair,1) && S.ild(LINE,4)-1==list(pair,2);
                    ild=S.ild(LINE,5);
                    GM=[GM;S.ild(LINE,6)];
                elseif S.ild(LINE,1)==date && S.ild(LINE,2)==site && S.ild(LINE,3)-1==list(pair,2) && S.ild(LINE,4)-1==list(pair,1);
                    ild=S.ild(LINE,5);
                    GM=[GM;S.ild(LINE,6)];
                end
            end
            itd=nan;
            for LINE=1:size(S.itd,1);
                if S.itd(LINE,1)==date && S.itd(LINE,2)==site && S.itd(LINE,3)-1==list(pair,1) && S.itd(LINE,4)-1==list(pair,2);
                    itd=S.itd(LINE,5);
                    GM=[GM;S.itd(LINE,6)];
                elseif S.itd(LINE,1)==date && S.itd(LINE,2)==site && S.itd(LINE,3)-1==list(pair,2) && S.itd(LINE,4)-1==list(pair,1);
                    itd=S.itd(LINE,5);
                    GM=[GM;S.itd(LINE,6)];
                end
            end
            ff=nan;
            for LINE=1:size(S.FF,1);
                if S.FF(LINE,1)==date && S.FF(LINE,2)==site && S.FF(LINE,3)-1==list(pair,1) && S.FF(LINE,4)-1==list(pair,2);
                    ff=S.FF(LINE,5);
                    GM=[GM;S.FF(LINE,6)];
                elseif S.FF(LINE,1)==date && S.FF(LINE,2)==site && S.FF(LINE,3)-1==list(pair,2) && S.FF(LINE,4)-1==list(pair,1);
                    ff=S.FF(LINE,5);
                    GM=[GM;S.FF(LINE,6)];
                end
            end
            el=nan;
            for LINE=1:size(S.El,1);
                if S.El(LINE,1)==date && S.El(LINE,2)==site && S.El(LINE,3)-1==list(pair,1) && S.El(LINE,4)-1==list(pair,2);
                    el=S.El(LINE,5);
                elseif S.El(LINE,1)==date && S.El(LINE,2)==site && S.El(LINE,3)-1==list(pair,2) && S.El(LINE,4)-1==list(pair,1);
                    el=S.El(LINE,5);
                end
            end
            az=nan;
            for LINE=1:size(S.Az,1);
                if S.Az(LINE,1)==date && S.Az(LINE,2)==site && S.Az(LINE,3)-1==list(pair,1) && S.Az(LINE,4)-1==list(pair,2);
                    az=S.Az(LINE,5);
                elseif S.Az(LINE,1)==date && S.Az(LINE,2)==site && S.Az(LINE,3)-1==list(pair,2) && S.Az(LINE,4)-1==list(pair,1);
                    az=S.Az(LINE,5);
                end
            end
            if ~isnan(z) || ~isnan(time(1)) || ~isnan(timeb(1)) || ~isnan(ild(1)) || ~isnan(itd(1)) || ~isnan(ff(1)) || ~isnan(az(1)) || ~isnan(el(1));
                data=[data;date,site,list(pair,1),list(pair,2),nanmean(GM),itd,ild,ff,az,el,timeb,time,z];
                disp(['date = ' num2str(date) '; site = ' num2str(site) '; unit1 = ' num2str(list(pair,1)) '; unit2 = ' num2str(list(pair,2))]);
            end
        end
    end
end

clear DATE D F FILES Tb timeb LINE GM N PAIR S SITE site T c date ff ild itd list noise pair time x z z1 z2 az el