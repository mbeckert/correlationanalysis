load('C:\Users\PenaLab\Desktop\FreeField\Analysis\Data\FreeFieldRawDATA.mat')
%%

filetype={'FF1','FF2','FF3','FF4','FF5','FF6','FFf1','FFf2','FFf3','FFf4','FFf5'};
FreeFieldDATA=cell(1,3);

for r = 1:3
    if r==1; data=OT; Right = [3,4,6]; end
    if r==2; data=FL; Right = 2; end
    if r==3; data=AAr; Right = [6,7,8,10]; end

    FreeFieldDATA{r}=[];

    for date=1:length(data)
        if sum(date==Right)
            R=1;
        else
            R=0;
        end
        for site = 1:length(data{date})

            for unit = 2:length(data{date}{site})
                d=data{date}{site}{unit};
                s_times=[];
                b_times=[];
                key=[date site unit-1];
                reps=[];
                frozen=[];

                for file = 1:length(filetype)
                    if isfield(d,filetype{file})
                        s_times=[s_times,d.(filetype{file}).spikes_times];
                        b_times=[b_times,d.(filetype{file}).base_times];
                        if file>6
                            f=file-6;
                        else
                            f=0;
                        end
                        reps=[reps,d.(filetype{file}).reps];
                        frozen=[frozen,f];
                    end % field check
                end % file

                if ~isempty(s_times)
                    idx=size(FreeFieldDATA{r},1)+1;
                    FreeFieldDATA{r}{idx,1}=key;
                    FreeFieldDATA{r}{idx,2}=b_times;
                    FreeFieldDATA{r}{idx,3}=s_times;
                    FreeFieldDATA{r}{idx,4}=reps;
                    FreeFieldDATA{r}{idx,5}=frozen;
                    FreeFieldDATA{r}{idx,6}=R;
                end

            end % unit

        end % site
    end % date

end % r

clear s_times b_times reps date site unit file filetype data key d r idx frozen f

%%

output=zeros(1,3);

for r=1:3;
    data=cellfun(@sum,FreeFieldDATA{r}(:,5));
    idx=data>0;

    key=cell2mat(FreeFieldDATA{r}(idx,1));
    data=FreeFieldDATA{r}(idx,3);

    for a=min(sort(unique(key(:,1)))):max(sort(unique(key(:,1))))
        for b=min(sort(unique(key(:,2)))):max(sort(unique(key(:,2))))
            A=find(key(:,1)==a);
            B=find(key(:,2)==b);
            idx=intersect(A,B);
            if ~isempty(idx)
                p=combnk(1:length(idx),2);
                output(r)=output(r)+size(p,1);
            end
        end
    end

end

clear a b A B data idx key r