load('C:\Users\mbeck\Desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted_2.mat')
%%

pattern = 'frozen.';
window = [0.1,0.15,0.2,0.3];

out = cell(length(window),3);



for R = 1:3
    switch R
        case 1
            data = OT;
            disp('Loading OT')
        case 2
            data = FL;
            disp('Loading FL')
        case 3
            data = AAr;
            disp('Loading AAr')
    end
    
    for w = 1:length(window)
        disp(['Window - ' num2str(window(w))])
        
        out{w,R} = nan(500,1);
        
        for date = 1:length(data)
            for site = 1:length(data{date})
                list = combnk(2:length(data{date}{site}),2);
                for pair = 1:size(list,1)
                    unit1 = fieldnames(data{date}{site}{list(pair,1)});
                    unit2 = fieldnames(data{date}{site}{list(pair,2)});
                    unit1 = unit1(~cellfun(@isempty,regexpi(unit1,pattern)));
                    unit2 = unit2(~cellfun(@isempty,regexpi(unit2,pattern)));
                    filenames = intersect(unit1,unit2);
                    clear unit1 unit2
                    
                    holder = nan(1,length(filenames));
                    disp(num2str(length(filenames)))
                    
                    for files = 1:length(filenames)
                        
                        unit1 = data{date}{site}{list(pair,1)}.(filenames{files}).spikes_times;
                        unit2 = data{date}{site}{list(pair,2)}.(filenames{files}).spikes_times;
                        
                        check = cellfun(@(x) x<window(w), unit1, 'UniformOutput', false);
                        for i = 1:length(check)
                            unit1{i}=unit1{i}(check{i});
                        end
                        check = cellfun(@(x) x<window(w), unit2, 'UniformOutput', false);
                        for i = 1:length(check)
                            unit2{i}=unit2{i}(check{i});
                        end
                        clear i check
                        
                        unit1 = cellfun(@length,unit1);
                        unit2 = cellfun(@length,unit2);
                        
                        c = corr(unit1,unit2);
                        holder(files) = 0.5*log((1+c)/(1-c));
                        
                    end     % FILES
                    
                    L = find(isnan(out{w,R}),1,'first');
                    out{w,R}(L)=nanmean(holder);
                    
                end         % PAIR
            end             % SITE
        end                 % DATE
        
        L = find(isnan(out{w,R}),1,'first');
        out{w,R}(L:end)=[];
        
    end                     % W
    
end                         % R

clearvars -except OT FL AAr out