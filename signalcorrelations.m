% load('Z:\SpeakerIndex.mat');

load('CURVES.mat');
load('KEYS.mat');

REGIONS={'AAr','OT','FL'};
TYPE={'ild','itd','FF','Az','El'};
POP={'out','in'};

% sect=cell(3,1);
% sect{1}=1:7; sect{2}=8:14; sect{3}=15:21;

C=[];
D=[];

% reps=60;

for pop=1:length(POP);
    for regions=1:length(REGIONS);
%     for regions=[1,3];
        for type=1:length(TYPE);
%         for type=[2,4];
            disp(['Running ' REGIONS{regions} ' ' TYPE{type} ' ' POP{pop}]);

            eval(['C=KEY_' (REGIONS{regions}) '.([TYPE{type}]);']);
            eval(['D=CURVE_' (REGIONS{regions}) '.([TYPE{type}]);']);

            data=[];
            last=0;
            for g=1:max(C(:,1));
                for h=1:max(C(:,2));
%                     C(C(:,5)<reps,:)=[];
%                     D(C(:,5)<reps,:)=[];
                    switch pop
                        case 1
                            date=find(C(last+1:end,1)~=g);
                            site=find(C(last+1:end,2)~=h);
                        case 2
                            date=find(C(:,1)==g);
                            site=find(C(:,2)==h);
                    end
                    key=intersect(date,site);
                    if max(key)>last;
                        last=key(end);
                    end
                    if ~isempty(key);
                        list=combnk(key,2);
                        for i=1:size(list,1);
                            holder=nan(1,10);
                            v1=D(list(i,1),:);
                            v2=D(list(i,2),:);
                            GM=sqrt(nanmean(v1)*nanmean(v2));
%                             v1=v1(randperm(length(v1)));
%                             v2=v2(randperm(length(v1)));
                            v1=smooth(D(list(i,1),:));
                            v2=smooth(D(list(i,2),:));
                            [CORR]=corrcoef(v1,v2,'rows','complete');
                            z=0.5*log((1+CORR(1,2))/(1-CORR(1,2)));
                            data=[data;C(list(i,1),1),C(list(i,1),2),C(list(i,1),3),C(list(i,2),3),z,GM];
                        end
                    end
                end
            end
            eval(['Corr_full_' (REGIONS{regions}) '_' (POP{pop}) '.' ([TYPE{type}]) '=data;']);
        end
    end  
end

clear C D CORR reps REGIONS regions TYPE type POP pop g h key list data z i last site date v1 v2 j holder sect
