%%
load('C:\Users\mbeck\Desktop\Data\Analysis\CorrelationsPaper\SynchRAWdata.mat');
%%

sample = 199;

for b = 1
    for s = 1:2
        switch s
            case 1
                d = SynchBase(:,b);
            case 2
                d = SynchEvoked(:,b);
        end
        
        out = cell(1,3);
        
        for R = 1:3
            out{R}{1} = nan(500,sample);
            out{R}{2} = nan(500,sample);
            out{R}{3} = nan(500,sample);
            filenames = fieldnames(d{R});
            for date = 1:10
                for site = 1:10
                    for unit1 = 1:10
                        for unit2 = 1:10
                            SmCCG = zeros(1,sample);
                            SmShiftCCG = zeros(1,sample);
                            SmCCGCorrected = zeros(1,sample);
                            counter = 0;
                            for file = 1:length(filenames)
                                key = d{R}.(filenames{file}){1};
                                i1 = find(key(:,1) == date);
                                i2 = find(key(:,2) == site);
                                i3 = find(key(:,3) == unit1);
                                i4 = find(key(:,4) == unit2);
                                idx = intersect(i1,(intersect(i2,(intersect(i3,i4)))));
                                if ~isempty(idx)
                                    counter=counter+1;
                                    SmCCG = SmCCG + nansum(cell2mat(d{R}.(filenames{file}){9}(idx,:)),2)';
                                    SmShiftCCG = SmShiftCCG + nansum(cell2mat(d{R}.(filenames{file}){10}(idx,:)),2)';
                                    SmCCGCorrected = SmCCGCorrected + nansum(cell2mat(d{R}.(filenames{file}){11}(idx,:)),2)';
                                end
                            end
                            if sum(SmCCGCorrected) ~= 0
                                L = find(isnan(out{R}{1}(:,1)),1,'first');
                                out{R}{1}(L,:) = SmCCGCorrected/counter;
                                out{R}{2}(L,:) = SmCCG/counter;
                                out{R}{3}(L,:) = SmShiftCCG/counter;
                            end
                        end
                    end
                end
            end
            L = find(isnan(out{R}{1}(:,1)),1,'first');
            out{R}{1}(L:end,:)=[];
            out{R}{2}(L:end,:)=[];
            out{R}{3}(L:end,:)=[];
        end
        
        switch s
            case 1
                B = out;
            case 2
                E = out;
        end
        
    end
    
    switch b
        case 1
            S01 = [B,E];
        case 2
            S02 = [B,E];
        case 3
            S05 = [B,E];
        case 4
            S10 = [B,E];
        case 5
            S20 = [B,E];
    end
    
end

clearvars -except S0* S10 S20 Synch*

%%

for b = 1
    switch b
        case 1
            data = S01;
        case 2
            data = S02;
        case 3
            data = S05;
        case 4
            data = S10;
        case 5
            data = S20;
    end
    
    for R = 1:3
    
        switch R
            case 1
                sample = 97;
            case 2
                sample = 92;
            case 3
                sample = 22;
        end
        
    for i = 1:length(sample)
    
    figure(1)
    plot(data{R}{1}(sample(i),:),'k')
    hold on
    plot(data{R}{2}(sample(i),:),'b--')
    plot(data{R}{3}(sample(i),:),'r--')
    title(['R = ' num2str(R) '; i = ' num2str(sample(i)) '; Spontaneous'])
    hold off
%     fig = gcf;
%     ax = fig.CurrentAxes;
%     a = ax.YLim;
    ylim([-2*10^-3 8*10^-3]);
    
    figure(2)
    plot(data{R+3}{1}(sample(i),:),'k')
    hold on
    plot(data{R+3}{2}(sample(i),:),'b--')
    plot(data{R+3}{3}(sample(i),:),'r--')
    title(['R = ' num2str(R) '; i = ' num2str(sample(i)) '; Evoked'])
%     fig = gcf;
%     ax = fig.CurrentAxes;
%     if ax.YLim(1)<a(1)
%         a(1) = ax.YLim(1);
%     end
%     if ax.YLim(2)>a(2)
%         a(2) = ax.YLim(2);
%     end
%         
%     figure(1); ylim(a); figure(2); ylim(a);
    
    hold off
    ylim([-2*10^-3 8*10^-3]);
    
    pause
    
    end
    
    end
    
end