load('decoderdata_mike_spiketimes.mat','decoderdata');

%%

DATA=cell(3,1);
DATA2=cell(3,1);

for region = 1:3;

    key=decoderdata{2,region};
    spiketimes=decoderdata{1,region};
    spikecount=cell(size(spiketimes));
    for i = 1:length(spikecount)
        spikecount{i}=cellfun(@length,spiketimes{i});
    end
    clear i spiketimes
    
    count=1;
    cluster=nan(size(key,1),3);
    cluster(:,2)=key(:,4);
    cluster(:,3)=key(:,5);
    for i1=1:max(key(:,1))
        for i2=1:max(key(:,2))
            idx1 = find(key(:,1)==i1);
            idx2 = find(key(:,2)==i2);
            idx = intersect(idx1,idx2);
            if ~isempty(idx)
                cluster(idx,1)=count;
                count=count+1;
            end
        end
    end
    clear i* count key

    idx=find(cluster(:,2)==1);
    spikecount=spikecount(idx);
    cluster=cluster(idx,:);

    idx=find(cluster(:,3)<100);
    spikecount=spikecount(idx);
    cluster=cluster(idx,:);

    clear idx

    DATA2{region}=spikecount;
    for i = 1:size(spikecount,1);
% % % % % %         x=zscore(spikecount{i}(:));
        x=spikecount{i}(:);
        DATA2{region}{i}=reshape(x,size(spikecount{i}));
%         for h = 1:size(spikecount{i},1);
%             DATA2{region}{i}(h,:)=zscore(spikecount{i}(h,:));
%         end
    end
        
    clear i x
    
    DATA2{region}=DATA2{region}';
    
    for c=1:max(cluster(:,1))

        data=spikecount(cluster(:,1)==c);

        if ~isempty(data)
            for i = 1:size(data,1)
                data1{i}=data{i}(:);
% % % % % % %                 data1{i}=zscore(data1{i});
%                 for h = 1:size(data{i},1);
% %                     data1{i}(h,:)=zscore(spikecount{i}(h,:));
%                     data1{i}(h,:)=spikecount{i}(h,:);
%                 end
%                 data1{i}=data{i}(:);
            end
            data1=cell2mat(data1)';

            DATA{region}{c}=nan(size(data{1}));
            for i = 1:length(data{1}(:))
                DATA{region}{c}(i)=mean(data1(:,i));
            end

        end
        clear data* i

    end

    clear c cluster spikecount h
    
end

RAWDATAcluster=DATA;
RAWDATAcell=DATA2;

clear DATA* region

%%

outputCluster=cell(3,1);
outputCell=cell(3,1);

for region = 1:3
    for c = 1:size(RAWDATAcluster{region},2)
        outputCluster{region}{c}=nan(size(RAWDATAcluster{region}{c},1),3);
        outputCluster{region}{c}(:,1)=mean(RAWDATAcluster{region}{c},2);
        outputCluster{region}{c}(:,2)=std(RAWDATAcluster{region}{c},0,2);
        outputCluster{region}{c}(:,3)=var(RAWDATAcluster{region}{c},0,2);
        outputCluster{region}{c}(:,4)=outputCluster{region}{c}(:,3)./outputCluster{region}{c}(:,1);
    end
end

for region = 1:3
    for c = 1:size(RAWDATAcell{region},2)
        outputCell{region}{c}=nan(size(RAWDATAcell{region}{c},1),3);
        outputCell{region}{c}(:,1)=mean(RAWDATAcell{region}{c},2);
        outputCell{region}{c}(:,2)=std(RAWDATAcell{region}{c},0,2);
        outputCell{region}{c}(:,3)=var(RAWDATAcell{region}{c},0,2);
        outputCell{region}{c}(:,4)=outputCell{region}{c}(:,3)./outputCell{region}{c}(:,1);
    end
end
    
clear region c
    
%%    

dataLISTcell=cell(3,1);
dataLISTcluster=cell(3,1);

for region = 1:3
    d=outputCell{region}';
    dataLISTcell{region}=cell2mat(d);
    d=outputCluster{region}';
    dataLISTcluster{region}=cell2mat(d);
end
    
clear region d

%%

for i =1:3;
    figure(i)
    scatter(data{i}(:,1),data{i}(:,3))
end

%%

maxCELL=cell(3,1);
maxCLUSTER=cell(3,1);

extract=4;

for region =1:3;
    maxCLUSTER{region}=nan(size(outputCluster{region},2),1);
    for c = 1:size(outputCluster{region},2)
        m=outputCluster{region}{c}(find(outputCluster{region}{c}(:,1)==max(outputCluster{region}{c}(:,1))),extract);
        if ~isempty(m);
            maxCLUSTER{region}(c,1)=max(m);
        end
    end
    maxCELL{region}=nan(size(outputCell{region},2),1);
    for c = 1:size(outputCell{region},2)
        m=outputCell{region}{c}(find(outputCell{region}{c}(:,1)==max(outputCell{region}{c}(:,1))),extract);
        if ~isempty(m);
            maxCELL{region}(c,1)=max(m);
        end
    end
end

clear c m region extract

%%

outputCluster=cell(3,1);
outputCell=cell(3,1);

for region = 1:3
    outputCluster{region}=nan(size(RAWDATAcluster{region},2),2);
    for c = 1:size(RAWDATAcluster{region},2)
        outputCluster{region}(c,1)=nanmean(RAWDATAcluster{region}{c}(:));
        outputCluster{region}(c,2)=nanstd(RAWDATAcluster{region}{c}(:));
    end
    outputCell{region}=nan(size(RAWDATAcell{region},2),2);
    for c = 1:size(RAWDATAcell{region},2)
        outputCell{region}(c,1)=nanmean(RAWDATAcell{region}{c}(:));
        outputCell{region}(c,2)=nanstd(RAWDATAcell{region}{c}(:));
    end
end

%%

cutoff=0.5;

for s = 1:2;

    if s==1; data=dataLISTcell; end
    if s==2; data=dataLISTcluster; end

    d{1}=data{1}(data{1}(:,1)>cutoff,:);
    d{2}=data{2}(data{2}(:,1)>cutoff,:);
    d{3}=data{3}(data{3}(:,1)>cutoff,:);


    if s==1; dataLISTcell_trunc=d; end
    if s==2; dataLISTcluster_trunc=d; end

end

clear d data s cutoff
