load('f:\desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted.mat');
data = OT; 
clear OT FL AAr

%%

output = cell(200,3);
M = cell(200,3);
chck = nan(200,3);

for d = 1:length(data)
    for s = 1:length(data{d})
        list = combnk(2:length(data{d}{s}),2);
        L = find(cellfun(@isempty,output) == 1 ,1 ,'first');
        for p = 1:size(list,1)
            for t = 1:3
                if isfield(data{d}{s}{list(p,1)},['FFf' num2str(t)]) && isfield(data{d}{s}{list(p,2)},['FFf' num2str(t)]) 

                    output{L+p-1,t} = diag(corr(data{d}{s}{list(p,1)}.(['FFf' num2str(t)]).spikes_count', data{d}{s}{list(p,2)}.(['FFf' num2str(t)]).spikes_count'));

                    M{L+p-1,t} = sqrt(mean(data{d}{s}{list(p,1)}.(['FFf' num2str(t)]).spikes_count,2) .* mean(data{d}{s}{list(p,1)}.(['FFf' num2str(t)]).spikes_count,2));
                    chck(L+p-1,t) = sqrt(data{d}{s}{list(p,1)}.(['FFf' num2str(t)]).base_mean + (2*data{d}{s}{list(p,1)}.(['FFf' num2str(t)]).base_std) * data{d}{s}{list(p,2)}.(['FFf' num2str(t)]).base_mean + (2*data{d}{s}{list(p,2)}.(['FFf' num2str(t)]).base_std));

                end % if check
            end     % t
        end         % p
    end             % s
end                 % d

L = find(cellfun(@isempty,output) == 1 ,1 ,'first');
output(L:end,:) = [];
M(L:end,:) = [];
chck(L:end,:) = [];

clear d s L list p t sp ho

for i = 1:length(M(:))
    if ~isempty(M{i})
        M{i}(M{i} < chck(i)) = nan;
    end
end

clear i chck

final = cell(size(output,1),1);

for i = 1:length(output)
    c = cell2mat(output(i,:));
    m = cell2mat(M(i,:));
    c = nanmean(c,2);
%     c = norm01(c')';
    m = nanmean(m,2);
    m = norm01(m')';
    final{i} = [m,c];
end

clear i c m

f = cell2mat(final);
scatter(f(:,1),f(:,2))
corr(f(:,1),f(:,2),'rows','pairwise')
