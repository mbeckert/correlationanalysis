load('C:\Users\PenaLab\Desktop\FreeField\Analysis\CorrelationsPaper\SignalCorrelationAz_Updated.mat','curve*');

%%
out=cell(1,3);                  % holder for the data output
bounds=[1:7; 8:14; 15:21];      % bounds for the curve in steps
X=0:10:20;                      % x values for the regression, change to match the unit and steps of the curve

for r = 1:3     % run through all three regions
    switch r    % "d" is the data structure for the analysis, switch between structures here
        case 1
            d=curve_ot;
        case 2
            d=sm_F;
        case 3
            d=sm_A;
    end


    out{r}=nan(size(d,1)*size(bounds,2)-2,3); % preallocate some space for the slopes

    for b = 1:3                 % run through each boundary
        count = 1;              % counter for where the next slope value should go
        for u = 1:size(d,1)     % run through each unit
            % run through all steps within the boundary
            for step = 1:length(~isnan(bounds(b,:)))-2 
                % calculate the regression
                coeff = polyfit(X,d(u,bounds(b,step):bounds(b,step+2)),1); 
                out{r}(count,b)=coeff(1); 
                count=count+1;
            end % step
        end % u
    end % b

end % r

clear bounds X r d b u count step coeff

%% plot the data

regions={'OT','FL','AAr'};
labels={'Ipsilateral','Frontal','Contralateral'};
X=2:2:6;

for r = 1:3
    d=cell(1,3);
    d{1}=out{r}(:,1);
    d{2}=out{r}(:,2);
    d{3}=out{r}(:,3);
    
%     lim=[min(cellfun(@min,d)) max(cellfun(@max,d))];
    
    Y=d;

    figure(r)
    boxplot2(X,Y)

    % some settings for the figure
    set(gca,'xtick',X,'xticklabel',labels,'ytick',-0.03:0.01:0.03)
    title(regions{r})
    box off
    xlim([0 X(end)+2])
    ylim([-0.03 0.035])

end

clear X Y labels r d