data=[];

load('SynchronyPlots.mat');

for i=1:size(FL{2,1},1);

SmCCGCorrected=FL{2,1}(i,5:end);

MAX=max(SmCCGCorrected);

lag=find(SmCCGCorrected==max(SmCCGCorrected));
if isempty(lag) || sum(isnan(lag))>0 || MAX==0;
    lag=100;
end
if length(lag)>1;
    tempMAX=lag;
    tempMAX=abs(tempMAX-100);
    selection=find(tempMAX==min(tempMAX),1,'first');
    lag=lag(selection);
end

if lag==199;
    tlag=198;
else
    tlag=lag;
end

positive=(SmCCGCorrected>=(MAX/2));
pos=diff(positive);
less=pos(1:tlag);
more=pos(tlag:end);
Low=find(less==1,1,'last');
Low=length(less)-Low;
High=find(more==-1,1,'first');
bandMAX=High+Low;

PEAK=SmCCGCorrected(100);

positive=(SmCCGCorrected>=(PEAK/2));
pos=diff(positive);
less=pos(1:100);
more=pos(100:end);
Low=find(less==1,1,'last');
High=find(more==-1,1,'first');
Low=length(less)-Low;
bandPEAK=High+Low;

if isempty(bandPEAK)
    bandPEAK=0;
end
if isempty(bandMAX)
    bandMAX=0;
end

AUC=sum(SmCCGCorrected);
if lag<6;
    aucMAX=sum(SmCCGCorrected(1:11))/11;
elseif lag > 195;
    aucMAX=sum(SmCCGCorrected(188:end))/11;
else
    aucMAX=sum(SmCCGCorrected(lag-5:lag+5))/11;
end
aucPEAK=sum(SmCCGCorrected(95:105))/11;

data=[data;MAX,bandMAX,aucMAX,lag-100,PEAK,bandPEAK,aucPEAK,AUC];

end