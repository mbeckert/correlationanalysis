
% TO DO:

% OT curves are too narrow
% make AAr curves have parallel curves in the front

%%

% create anonymous functions to generate ot and aar tuning curves
gauss = @(x,b,c) exp(-(((x-b).^2)/((2*c).^2)));
x=-1:0.01:1;
sine = @(a) sin(-a:a/100:a);

% create hypothetical distributions for the tuning curve position and shape
% for ot {1} and aar {2}. This provides an overrepresentation of frontal
% space for ot and an even distribution for aar.
dist{1}=logspace(0,1,100)/10;
dist{2}=linspace(1,5,100);

ot = cell(length(dist{1}),1);
aar = cell(length(dist{2}),1);

% generate the curves
for i = 1:length(dist{1})
    ot{i} = gauss(x,dist{1}(i),0.2*dist{1}(i));
    aar{i} = sine(dist{2}(i));
end

clear x b c gauss sine i dist

%% Run the correlation analysis for the curves

% this will create the combinations but all remove pairs that are "close"
% to one another. change the '10' in line 47 to whatever arbitraty value
% seems to be close to what "nearby" cells from the tetrodes would fall
% under.
list = combnk(1:100,2);
list = list(abs(list(:,2)-list(:,1))>10,:);

r=nan(length(list),2);

% get R values
for i = 1:length(list);
    r(i,1)=corr(ot{list(i,1)}',ot{list(i,2)}');
    r(i,2)=corr(aar{list(i,1)}',aar{list(i,2)}');
end

% convert to z
z=0.5.*log((1+r)./(1-r));

clear i list

%% Plots

figure(1)
subplot(1,2,1)
hold on
for i = 1:100;
    plot(ot{i})
end
hold off
subplot(1,2,2)
hist(z(:,1))
xlim([-0.5 4])

figure(2)
subplot(1,2,1)
hold on
for i = 1:100;
    plot(aar{i})
end
hold off
subplot(1,2,2)
hist(z(:,2))
xlim([-0.5 4])

clear i